#!/usr/bin/env bash
set -eu

sed '/<!DOCTYPE html>/,/<!--TITLE BAR END-->/!d' src/main.html \
    | sed 's|../css/sca.css">|../css/sca.css">\n    <link rel="stylesheet" href="../css/syntax.css">|' \
    | sed '/<nav>/,\|</nav>|d' \
    | sed 's/SCA++/SCA++ - Help/' \
    | sed 's|src="../js/page.js"|src="../js/syntax.js"|' \
    > src/syntax.html
{
    echo '<main>'
    pandoc ../doc/syntax.md
    echo -e '</main>\n</body>\n</html>'
} >> src/syntax.html
exit 0
