<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Security-Policy"
          content="default-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; style-src 'self' 'unsafe-inline'">
    <title>SCA++ - Help</title>
    <link rel="stylesheet" href="../css/sca.css">
    <link rel="stylesheet" href="../css/syntax.css">
    <link rel="stylesheet" href="../node_modules/codemirror/lib/codemirror.css">
    <script src="../node_modules/codemirror/lib/codemirror.js"></script>

    <!-- Do NOT change the relative order of these! -->
    <script defer src="./electron/base.js"></script>
    <script defer src="../js/syntax.js"></script>
    <script defer src="./electron/menu.js"></script>
</head>
<body>
<div id="title-bar">
    <div id="title-bar-text">
        <div id="title-bar-text-content">SCA++ - Help</div>
    </div>
    <div id="window-controls">
        <div id="minimise-button" title="Minimise">
            <svg id="minimise-button-icon" width="1cm" height="1cm" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <path d="M 50 30 L 35 45 L 50 60 L 65 45 L 50 30"/>
                <path d="M 50 35 L 40 45 L 50 55 L 60 45 L 50 35" class="transparent"/>
                <path d="M 0 0 H 100 V 45 H 0 V 0" class="transparent"/>
            </svg>
        </div>
        <div id="maximise-restore-button" title="Maximise">
            <svg id="maximise-restore-button-icon" width="1cm" height="1cm" viewBox="0 0 100 100"
                 xmlns="http://www.w3.org/2000/svg">
                <path d="M 50 35 L 35 50 L 50 65 L 65 50 L 50 35" class="restore"/>
                <path d="M 50 40 L 40 50 L 50 60 L 60 50 L 50 40" class="transparent restore"/>
                <path d="M 50 40 L 35 55 L 50 70 L 65 55 L 50 40" class="maximise"/>
                <path d="M 50 45 L 40 55 L 50 65 L 60 55 L 50 45" class="transparent maximise"/>
                <path d="M 0 55 H 100 V 100 H 0 V 55" class="transparent maximise"/>
            </svg>
        </div>
        <div id="close-button" title="Close">
            <svg id="close-button-icon" width="1cm" height="1cm" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <path d="M 40 38 L 62 60 L 60 62 L 38 40"/>
                <path d="M 60 38 L 38 60 L 40 62 L 62 40"/>
            </svg>
        </div>
    </div> <!--WINDOW CONTROLS END-->
</div> <!--TITLE BAR END-->
<main>
<h1 id="sca-syntax">SCA++ Syntax</h1>
<p>This page documents the syntax used by SCA++.</p>
<h2 id="overview">Overview</h2>
<p>SCA++ uses three input boxes, one for phoneme classes, one for rules
(= sound changes), and one for the words that the changes should be
applied to.</p>
<h3 id="general-remarks">General Remarks</h3>
<p>Whitespace in class definitions and rules is ignored; for
example,</p>
<pre class="sca++"><code>a/b/_
a /                 b/    _</code></pre>
<p>both of these rules are equivalent.</p>
<p>In the examples in this guide, remarks enclosed in brackets
<code>()</code>, unless otherwise noted, are comments and serve only
documentation purposes. They are not part of the syntax of SCA++.</p>
<h3 id="cheat-sheet">Cheat Sheet</h3>
<div>
<h4 id="basic-usage">Basic Usage</h4>
<pre class="sca++"><code>t / d / _                 (ta &gt; da)
p, t, kʷ / b, d, gʷ / _   (pa, ta, kʷa &gt; ba, da, gʷa)
p, t, kʷ / b / _          (pa, ta, kʷa &gt; ba, ba, ba)</code></pre>
<h4 id="environment">Environment</h4>
<pre class="sca++"><code>t / d / _a   (ta, te &gt; da, te)  
a / i / t_   (ta, da &gt; ti, da)</code></pre>
</div>
<h4 id="word-boundaries">Word boundaries</h4>
<pre class="sca++"><code>d / t / _#   (da, ad &gt; da, at)  
d / t / #_   (da, ad &gt; da, at)</code></pre>
<h4 id="optional-and-negated-elements">Optional and negated
elements</h4>
<pre class="sca++"><code>d / t / _(a)r   (der, dar, dr &gt; der, tar, tr)  
d / t / _~[a]   (da, de &gt; da, te)</code></pre>
<h4 id="wildcards">Wildcards</h4>
<pre class="sca++"><code>d / t / _*r   (dr, dar, d桜r &gt; dr, tar, t桜r)</code></pre>
<h2 id="rules">Rules</h2>
<p>There are five types of rules, each of which use a slightly different
syntax.</p>
<p>Each rule must be on a single line.</p>
<h3 id="substitution-rules">Substitution Rules</h3>
<p>Substitution rules have an input and output. Their syntax is:</p>
<pre class="sca++"><code>input/output/context</code></pre>
<p>NOTE: The first <code>/</code> can also be a <code>&gt;</code> if
that’s what you prefer. Semantically, there is no difference between the
two.</p>
<p>Examples:</p>
<pre class="sca++"><code>p / b / vowel_vowel         (p &gt; b between vowels)
pp, tt, kk / p, t, k /_#    (word-final voiceless stops degeminate)</code></pre>
<h4 id="input">Input</h4>
<p>The input of a substitution rule consists of comma-separated
characters, classes, and combinations thereof.</p>
<p>Examples:</p>
<pre class="sca++"><code>a               (matches the character `a`)        
abc             (matches the characters `abc` in a row)              
a, b, c         (matches either `a`, or `b`, or `c`)          
ad, bd, cd      (matches either `ad`, or `bd`, or `cd`)     
{ a, b, c }d    (first matches either `a`, `b`, or `b`, and then `d`)     
{ a, b, c }d, f (matches either the same as the previous line, or a single `f`) </code></pre>
<p>Note that lines 4 and 5 are equivalent. Both do the same thing, it’s
just written differently.</p>
<p>The wildcard operator <code>*</code> may be used in place of a
character and matches <em>any</em> one character.</p>
<h4 id="output">Output</h4>
<p>The output of a substitution rule is just like the input, except
that:</p>
<ol type="1">
<li><p>The output must contain the same number of elements as the
input:</p>
<pre class="sca++"><code>a, b, c / e, f, g / _             (OK)
{ a, b, { c }} / e, { f, g } / _  (OK, classes are flattened)
{a, b, c}d / abcd, fegh, i / _    (OK, how complex each element is doesn&#39;t matter)
a, b / e, f, g / _                (Error: too many output elements)
a, b, c / e, f / _                (Error: not enough output elements)</code></pre>
<p>The only exception to this is when you have exactly one output
element, in which case you can have as many inputs as you like:</p>
<pre class="sca++"><code>   a, b, c / d / _   (OK, `a`, `b`, and `c` all become `d`)</code></pre></li>
<li><p>The wildcard operator may not be used in the output (because what
would that even mean?).</p></li>
<li><p>Percentages can be used to introduce irregularity: an element may
be replaced with a class that contains percentage-qualified
elements:</p>
<pre class="sca++"><code>a, b, c / e, f, %{ 20%g, 40%h, r }</code></pre>
<p>More on percentages later on.</p></li>
</ol>
<h4 id="context">Context</h4>
<p>The context is the same for all rules and determines what must
precede or follow the input for a rule to apply to it. The syntax of the
context is as follows:</p>
<ol type="1">
<li><p>The context must contain exactly one underscore:</p>
<pre class="sca++"><code>a / b / _    (OK)
a / b / ___  (OK, multiple consecutive underscores are treated as one)
a / b /      (Error: no underscore in context)
a / b / _c_  (Error: multiple non-consecutive underscores in context)  </code></pre>
<p>A context containing only an underscore means that the input will be
replaced with the output wherever it occurs. For example, the rule</p>
<pre class="sca++"><code>a / b / _</code></pre>
<p>means ‘every occurrence of <code>a</code> is replaced with
<code>b</code>’.</p></li>
<li><p>The underscore may be preceded or followed by characters and
classes. These indicate that the input muse be preceded or followed by
those characters and classes for a substitution to take place. The
characters and classes that are part of the context themselves are not
replaced. For example, the rule</p>
<pre class="sca++"><code>a / b / c_d</code></pre>
<p>means ‘<code>a</code> becomes <code>b</code> between <code>c</code>
and <code>d</code>, but <code>c</code> and <code>d</code> remain
unchanged’.</p></li>
<li><p>A <code>#</code> sign at the very beginning or end of whatever
comes before or after the underscore indicates a word boundary. For
example</p>
<pre class="sca++"><code>a / b / #_   (OK, a &gt; b at the beginning of a word)
a / b / _#   (OK, a &gt; b at the end of a word)
a / b / #_#  (OK, a &gt; b if the word is ‘a’)
a / b / _c#  (OK, a &gt; b if followed by ‘c’ at the end of a word)
a / b / _#c  (Error: characters after the end of a word are not allowed)
a / b / c#_  (Error: characters before the beginning of a word are not allowed)</code></pre></li>
<li><p>The <code>~[]</code> operator negates an element. This means a
rule only applies if it <em>doesn’t</em> contain that element at that
position.</p>
<pre class="sca++"><code>a / b / ~[c]_        (OK, a &gt; b unless preceded by ‘c’)
a / b / _~[c]        (OK, a &gt; b unless followed by ‘c’)
a / b / _~[{c, d}]e  (OK, a &gt; b unless followed by ‘ce’, or ‘de’)
a / b / ~[]_         (Error, ‘~[]’ must contain  an element)</code></pre></li>
<li><p>Brackets <code>()</code> may be used to indicate optional
elements, which may, but need not, be present:</p>
<pre class="sca++"><code>a / b / _(c)e       (OK, a &gt; b before ‘e’ or ‘ce’)
a / b / _({c, d})e  (OK, a &gt; b before ‘e’, or ‘ce’, or ‘de’)</code></pre></li>
</ol>
<h3 id="epenthesis-rules">Epenthesis Rules</h3>
<p>Epenthesis Rules are just like substitution rules, except that they
have no input, and their output may contain only one element:</p>
<pre class="sca++"><code>/ a / b_       (OK, insert ‘a’ after every ‘b’)
/ e / #_s      (OK, insert ‘e’ before word-initial ‘s’)
/ a / _        (OK, insert ‘a’ absolutely everywhere (not recommended))
/ a, b / _     (Error: the output of an epenthesis rule may contain only one element)
/ {a, b}c / _  (Error: same as previous line, since this expands to `ac, bc`)</code></pre>
<h3 id="deletion-rules">Deletion Rules</h3>
<p>Deletion rules are the opposite of epenthesis rules: they have no
output. However, their input may consist of more than one element:</p>
<pre class="sca++"><code>a // b_    (OK, yeet ‘a’ before ‘b’)
e // _#    (OK, yeet word-final ‘e’)
a // _     (OK, yeet ‘a’ everywhere)
a, e // _  (OK, yeet ‘e’ and ‘a’ everywhere)
// _       (Error, empty deletion rule)</code></pre>
<p>Again, whitespace doesn’t matter, so whether you use <code>//</code>
or <code>/ /</code> here is up to you.</p>
<h3 id="metathesis-rules">Metathesis Rules</h3>
<p>Metathesis rules are identified by their ‘output’ consisting of
<code>&amp;</code>. A metathesis rule reverses each input element.
Diacritics remain attached to the preceding character:</p>
<pre class="sca++"><code>st / &amp; / _       (OK, ‘st’ becomes ‘ts’)
st, zd / &amp; / _#  (OK, ‘st’ and ‘zd’ become ‘ts’ and ‘dz’ word-finally)
ɑ̃n̩e / &amp; / s_     (OK, ‘ɑ̃n̩e’ becomes ‘en̩ɑ̃’ after ‘s’)</code></pre>
<h3 id="reduplication-rules">Reduplication Rules</h3>
<p>Reduplication rules are identified by their ‘output’ consisting of
one or more <code>+</code> signs. The input elements are repeated n
times, where n is the number of <code>+</code> signs:</p>
<pre class="sca++"><code>p, t, k / + / #_  (OK, geminate word-initial ‘p’, ‘t’, ‘k’)
s / ++++ / _      (OK, ‘s’ becomew ‘sssss’)
st / + / _        (OK, ‘st’ becomes ‘stst’)</code></pre>
<h2 id="classes">Classes</h2>
<p>Classes can be defined in the ‘Classes’ input box, in which case they
are assigned a name and can be referred to by that name in rules and
following definitions.</p>
<p>The syntax for a class definition is as follows:</p>
<pre class="sca++"><code>class-name = { characters }</code></pre>
<p>The class name consists of one or multiple characters and may contain
any character that doesn’t have special meaning (like <code>#</code> or
<code>/</code>).</p>
<p>The characters inside the class definition are sequences of
characters that are separated by commas. You can also define classes in
terms of other classes:</p>
<pre class="sca++"><code>front               = { i, e }
back                = { u, o }
vowels              = { front, back }</code></pre>
<p>In the example above, the classes <code>front</code> and
<code>back</code> in the definition of <code>vowels</code> are expanded
right then and there, yielding <code>{ i, e, u, o }</code>.</p>
<h3 id="using-classes-in-rules">Using Classes in Rules</h3>
<p>Classes denote alternatives and normally simply expand to their
containing elements. The following are all equivalent:</p>
<pre class="sca++"><code>{a, b, c}
{{a, b, c}}
{{a, b}, c}
{a, {b, c}}
{{a}, b, c}
{a, {b, {c}}}
{ {{a}}, {{{{ b, {{c}} }}}} }</code></pre>
<p>As we have done multiple times already, we can also use classes
directly in a rule without assigning them a name first. For example,
assuming <code>vowels</code> is defined as above, the rules below are
equivalent:</p>
<pre class="sca++"><code>ai, ae, au, ao / a, b, c, d / _
a{vowels} / a, b, c, d / _
a{i, e, u, o} / a, b, c, d / _ </code></pre>
<p><strong>IMPORTANT:</strong> Class names <em>must</em> be separated
from surrounding characters that do not have special meaning (like
<code>{</code> or <code>/</code>) by an extra pair of <code>{}</code>.
If you were to write <code>avowels</code> rather than
<code>a{vowels}</code>, it would interpret <code>avowels</code> either
as the name of a class, or, since we haven’t defined any class with that
name, as the character sequence <code>a v o w e l s</code>.</p>
<p>For example, assuming we have the following class definitions:</p>
<pre class="sca++"><code>FS  = { a, b }
SR  = { b, c }
FSR = { o, p, q }</code></pre>
<p>We can use them as follows:</p>
<pre class="sca+++"><code>FSR    (equivalent to ‘{ o, p, q }’)
{FS}R  (equivalent to ’{ a, b }R’)
F{SR}  (equivalent to ’F{ b, c }’)</code></pre>
<h3 id="definition-order">Definition Order</h3>
<p>Class definitions are processed top to bottom. The following is
valid, but does not do what you might think it does:</p>
<pre class="sca++"><code>vowels       = { front, back }
front        = { i, e }
back         = { u, o }
vowels-or-q  = { vowels, q }       </code></pre>
<p>In this case, <code>vowels</code> is defined in terms of
<code>front</code> and <code>back</code>, but <code>front</code> and
<code>back</code> are not defined yet and are just treated as the
character sequences <code>f r o n t</code> and <code>b a c k</code>. The
<code>vowels</code> class is thus equivalent to
<code>{f, r, o, n, t, b, a, c, k}</code></p>
<p>This is because a class definition is expanded as soon as it is
encountered. Here’s another example. Consider the definition of
<code>vowels-or-q</code> above. In it, we’re using the
<code>vowels</code> class, which we defined in terms of
<code>front</code> and <code>back</code>.</p>
<p>However, <code>front</code> and <code>back</code> in the definition
of <code>vowel</code> will always have the meaning that they had <em>at
the time <code>vowels</code> was defined</em>. This means that that
<code>vowels-or-t</code> is NOT defined as
<code>{ i, e, u, o, t }</code>, but rather as
<code>{f, r, o, n, t, b, a, c, k, q}</code></p>
<p>This behaviour is necessary, because otherwise, the following might
lead to complications:</p>
<pre class="sca++"><code>a = { b }
b = { a }</code></pre>
<p>If forward references to classes were allowed, this would lead to
problems: in the example above, we would be defining <code>a</code> in
terms of <code>b</code>, we’re defining in terms of <code>a</code>,
which we’re defining in terms of <code>b</code> and so on. It would
never stop.</p>
<p>This is why class definitions are processed in order. Doing so solves
this problem: In the example above, the class <code>a</code> is defined
as being a class containing only the <em>character</em> <code>b</code>.
And the class <code>b</code> is then defined to be the same as the class
<code>a</code>.</p>
<h3 id="operators">Operators</h3>
<p>Due to the fact that classes are very similar to sets, we can apply
set-theoretical operations to them to construct new classes.</p>
<h4 id="the-difference-operator">The Difference Operator</h4>
<p>The binary <code>~</code> operator is used to construct new classes
by removing characters from a class. It’s left-hand side should be a
class, but its right-hand side may be either a class or simply a
character. Assuming <code>FS</code>, <code>SR</code>, and
<code>FSR</code> are defined like so:</p>
<pre class="sca++"><code>FS  = { a, b }
SR  = { b, c }
FSR = { o, p, q }</code></pre>
<p>We then get:</p>
<pre class="sca++"><code>FSR~o       (Equivalent to ‘{ p, q }’)
FSR~d       (No effect since ‘FSR’ doesn&#39;t contain ‘d’; same as ‘FSR’)
FSR~{o, p}  (Equivalent to ‘{q}’)
FS~SR       (Equivalent to ‘{a}’)</code></pre>
<p>The reason why this is called the ‘difference’ operator is because,
it computes set difference between two classes.</p>
<h4 id="other-operators">Other operators</h4>
<p>A detailed explanation of all of these will be provided in the near
future.</p>
<p>The <code>*</code> operator computes the cartesian product of two
classes.<br />
The <code>+</code> operator concatenates classes element by
element.<br />
The <code>|</code> operator computes the union of two classes.<br />
The <code>&amp;</code> operator computes the intersection of two
classes</p>
<h2 id="grammar-specification">Grammar Specification</h2>
<p>This section is intended as a formal specification of the syntax of
SCA++. You probably want to skip it if you’re not a programmer.</p>
<p>Terminals are in all-caps and are not further elaborated on in here.
See <code>lib/parser.hh</code> for a list of all tokens, which more or
less correspond to the terminals.</p>
<pre class="bnf"><code>&lt;rule&gt; ::= &lt;substitution-rule&gt;
         | &lt;epenthesis-rule&gt;
         | &lt;deletion-rule&gt;
         | &lt;metathesis-rule&gt;
         | &lt;reduplication-rule&gt;
         
&lt;class-def&gt;  ::= TEXT [ &quot;=&quot; ] &lt;simple-el&gt;

&lt;substitution-rule&gt;  ::= &lt;input&gt; SEPARATOR &lt;output&gt;    &lt;context&gt;
&lt;epenthesis-rule&gt;    ::=         SEPARATOR &lt;output&gt;    &lt;context&gt;
&lt;deletion-rule&gt;      ::= &lt;input&gt; SEPARATOR             &lt;context&gt;
&lt;metathesis-rule&gt;    ::= &lt;input&gt; SEPARATOR &quot;&amp;&quot;         &lt;context&gt;
&lt;reduplication-rule&gt; ::= &lt;input&gt; SEPARATOR &quot;+&quot; { &quot;+&quot; } &lt;context&gt;

&lt;input&gt;      ::= &lt;input-els&gt;  { &quot;,&quot; &lt;input-els&gt; }
&lt;input-els&gt;  ::= { &lt;input-el&gt; }+
&lt;input-el&gt;   ::= &lt;simple-el&gt; | &quot;*&quot;

&lt;output&gt;     ::= &lt;output-els&gt; { &quot;,&quot; &lt;output-els&gt; }
&lt;output-els&gt; ::= { &lt;output-el&gt; }+
&lt;output-el&gt;  ::= &lt;percent-alternatives&gt; | &lt;simple-el&gt;
          
&lt;context&gt;    ::= SEPARATOR [ &lt;ctx-els&gt; ] { USCORE }+ [ &lt;ctx-els&gt; ] EOL
&lt;ctx-els&gt;    ::= &lt;decorated-els&gt; 

&lt;decorated-els&gt; ::= { &lt;decorated-el&gt; }+
&lt;decorated-el&gt;  ::= &lt;simple-el&gt; 
                  | &lt;boundaries&gt; 
                  |     &quot;(&quot;  &lt;decorated-els&gt; &quot;)&quot;
                  | &quot;~&quot; &quot;[&quot;  &lt;decorated-els&gt; &quot;]&quot;
&lt;boundaries&gt;    ::= { &quot;#&quot; | &quot;$&quot; }

&lt;percent-alternatives&gt; ::= PERCENTAGE &lt;percent-class&gt;           
&lt;percent-class&gt;        ::= &quot;{&quot; &lt;percent-list&gt; &quot;}&quot;
&lt;percent-list&gt;         ::= &lt;percent-els&gt; { &quot;,&quot; &lt;percent-els&gt; }
&lt;percent-els&gt;          ::= [ PERCENTAGE ] { &lt;percent-el&gt; }+
&lt;percent-el&gt;           ::= ( TEXT | &lt;percent-class&gt; )  
                             
&lt;simple-el&gt;            ::= TEXT | &lt;simple-el-class&gt;
&lt;simple-el-class&gt;      ::= &lt;simple-el-class-lit&gt; { &lt;set-op&gt; &lt;simple-el-rhs&gt; }
&lt;simple-el-rhs&gt;        ::= &lt;simple-el-class-lit&gt; | TEXT
&lt;simple-el-class-lit&gt;  ::= CLASS-NAME | &quot;{&quot; &lt;simple-el-list&gt; &quot;}&quot;
&lt;simple-el-list&gt;       ::= &lt;simple-els&gt; { &quot;,&quot; &lt;simple-els&gt; }
&lt;simple-els&gt;           ::= { &lt;simple-el&gt; }+
&lt;set-op&gt;               ::= &quot;~&quot; | &quot;&amp;&quot; | &quot;*&quot; | &quot;|&quot;</code></pre>
</main>
</body>
</html>
