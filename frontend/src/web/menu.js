if (window.electron) throw new Error('This file may not be loaded in Electron-mode')

window.menu = {
    ///
    /// File
    ///
    download(text, filename, mime = 'application/json') {
        /// Convert the data to a blob.
        const b = new Blob([text], {type: mime})
        const u = URL.createObjectURL(b)

        /// Download it.
        const a = document.createElement('a')
        a.setAttribute('href', u)
        a.setAttribute('download', filename)
        a.style.display = 'none'
        document.body.appendChild(a)
        a.click()

        /// Cleanup.
        URL.revokeObjectURL(u)
        a.remove()
    },
    async new() {
        let yes = await Dialog.open_confirm_dialog('Unsaved Changes', 'You have unsaved changes.<br><br>Create a new session anyway?')
        if (yes) new_session()
        return yes
    },
    open() {
        const i = document.createElement('input')
        i.type = 'file'
        i.accept = 'application/json'
        i.onchange = e => {
            /// Get the file handle and remove the input element.
            const file = e.target?.files?.item(0)
            i.remove()
            if (!file) return

            /// Get the file contents.
            const reader = new FileReader()
            reader.onload = async () => {
                let result = reader.result;
                if (!result) return
                try {
                    if (result instanceof ArrayBuffer) result = (new TextDecoder()).decode(result);
                    const data = JSON.parse(result)
                    for (let cm of code_mirrors) eval(`${cm}.setValue(data['${cm}'] ?? '')`)
                } catch (e) {
                    console.error(e)
                    Dialog.error(`Could not load session.<br><br>The specified file does not appear to contain a valid session: ${e.message}`)
                }
            };
            reader.readAsText(file)
        };
        i.click()
    },
    save() {
        let data = {}
        for (let cm of code_mirrors) eval(`data.${cm} = ${cm}.getValue()`)
        this.download(JSON.stringify(data), 'sca-session.sca.json')
    },
    export() {
        this.download(results_as_csv(), 'sca-results.csv', 'text/csv')
    },

    ///
    /// Help
    ///
    syntax() { window.open('/tools/sca/syntax', '_blank').focus() }
}