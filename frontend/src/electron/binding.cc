#include "../../../lib/applier.hh"

#include </usr/include/node/node_api.h>

#define NAPI_CALL(call, ...)                                      \
    do {                                                          \
        napi_status status = (call);                              \
        if (status != napi_ok) {                                  \
            const napi_extended_error_info* error_info = nullptr; \
            napi_get_last_error_info((env), &error_info);         \
            const char* err_message = error_info->error_message;  \
            bool        is_pending;                               \
            napi_is_exception_pending((env), &is_pending);        \
            if (!is_pending) {                                    \
                const char* message = (err_message == nullptr)    \
                                          ? "(null)"              \
                                          : err_message;          \
                napi_throw_error((env), nullptr, message);        \
            }                                                     \
            return __VA_ARGS__ nullptr;                           \
        }                                                         \
    } while (0)

#define NAPI_RAISE(msg, ...)                   \
    do {                                       \
        napi_throw_error(env, nullptr, (msg)); \
        return __VA_ARGS__ nullptr;            \
    } while (0)

#define NAPI_RAISE_TYPE_ERROR(msg, ...)             \
    do {                                            \
        napi_throw_type_error(env, nullptr, (msg)); \
        return __VA_ARGS__ nullptr;                 \
    } while (0)

#define NAPI_PARAMS(count)                                                                                               \
    u64        argc = count;                                                                                             \
    napi_value argv[count];                                                                                              \
    NAPI_CALL(napi_get_cb_info(env, info, &argc, argv, nullptr, nullptr));                                               \
    if (argc < count) NAPI_RAISE(fmt::format("Internal Error: Illegal Function Call: {} requires at least {} arguments", \
        __PRETTY_FUNCTION__, count)                                                                                      \
                                     .c_str());

#define NAPI_ASSERT_TYPE(v, vt)                                \
    do {                                                       \
        if (!assert_type_or_raise(env, v, vt)) return nullptr; \
    } while (0)

#define NAPI_GET_U32STRING(out, value)                                      \
    do {                                                                    \
        if (!get_u32string_from_js_string(out, env, value)) return nullptr; \
    } while (0)

#define NAPI_CALLBACK(name, param_count)                         \
    napi_value name(napi_env env, napi_callback_info info) try { \
    NAPI_PARAMS(param_count)

#define NAPI_CALLBACK_END()                                             \
    }                                                                   \
    catch (const assertion_error& e) {                                  \
        NAPI_RAISE(e.what());                                           \
    }                                                                   \
    catch (const std::exception& e) {                                   \
        NAPI_RAISE(fmt::format("Runtime Error: {}", e.what()).c_str()); \
    }                                                                   \
    catch (...) {                                                       \
        NAPI_RAISE("An unknown error has occurred");                    \
    }

#define NAPI_EXPORT_WITH_USERDATA(name, val)                                               \
    do {                                                                                   \
        napi_value _f;                                                                     \
        NAPI_CALL(napi_create_function(env, STR(name), NAPI_AUTO_LENGTH, name, val, &_f)); \
        NAPI_CALL(napi_set_named_property(env, module, STR(name), _f));                    \
    } while (0)

#define NAPI_EXPORT(name) NAPI_EXPORT_WITH_USERDATA(name, nullptr)

#define IN_ENUM(value, enum) ((value) >= static_cast<decltype(value)>(enum ::$$least) && (value) < static_cast<decltype(value)>(enum ::$$count))

enum struct sca_mode : i64 {
    $$least = 0,
    normal  = $$least,
    zompist = 1,
    $$count,
};

const char* valuetype_to_string(napi_valuetype vt) {
    switch (vt) {
        case napi_undefined: return "undefined";
        case napi_null: return "null";
        case napi_boolean: return "boolean";
        case napi_number: return "number";
        case napi_string: return "string";
        case napi_symbol: return "symbol";
        case napi_object: return "object";
        case napi_function: return "function";
        case napi_external: return "external";
        case napi_bigint: return "bigint";
    }
    return "invalid";
}

bool assert_type_or_raise(napi_env env, napi_value v, napi_valuetype expected) {
    napi_valuetype vt;
    NAPI_CALL(napi_typeof(env, v, &vt), (bool) );
    if (vt != expected) {
        NAPI_CALL(napi_throw_type_error(env, nullptr, fmt::format("Expected arguments of type {}, but got {}", valuetype_to_string(expected), valuetype_to_string(vt)).c_str()), (bool) );
        return false;
    }
    return true;
}

bool get_u32string_from_js_string(std::u32string& out, napi_env env, napi_value v) {
    /// Get the length of the string.
    std::string buf;
    u64         required = 0, written = 0;
    NAPI_CALL(napi_get_value_string_utf8(env, v, nullptr, 0, &required), (bool) );

    /// Make sure we have enough space and get the string.
    buf.resize(required + 1);
    NAPI_CALL(napi_get_value_string_utf8(env, v, buf.data(), required + 1, &written), (bool) );

    /// Convert it to utf-32. This might raise a range_error if we somehow got invalid utf-8.
    try {
        out = sca::to_utf32(buf);
        if (auto pos = out.find(U'\0'); pos != std::u32string::npos) out.erase(pos);
    } catch (const std::range_error& e) { NAPI_RAISE(fmt::format("Error converting UTF-8 string: {}", e.what()).c_str(), (bool) ); }
    return true;
}

NAPI_CALLBACK(parse_and_apply, 4) {
    /// Make sure the parameters are strings.
    for (u64 i = 0; i < 3; i++) NAPI_ASSERT_TYPE(argv[i], napi_string);

    /// The last parameter must be a number.
    NAPI_ASSERT_TYPE(argv[3], napi_number);

    /// Convert the js strings to utf-32 strings.
    std::u32string parser_inputs[3];
    for (u64 i = 0; i < 3; i++) NAPI_GET_U32STRING(parser_inputs[i], argv[i]);

    /// Convert the last parameter to a mode.
    int64_t mode_raw;
    NAPI_CALL(napi_get_value_int64(env, argv[3], &mode_raw));
    if (!IN_ENUM(mode_raw, sca_mode)) NAPI_RAISE_TYPE_ERROR(fmt::format("SCA Mode must be between {} and {}, but was {}", i64(sca_mode::$$least), i64(sca_mode::$$count) - 1, mode_raw).c_str());
    auto mode = static_cast<sca_mode>(mode_raw);

    /// Set up the parser and redirect diagnostics.
    std::string diagnostics;
    sca::parser p;
    p.diagnostic_handler = [&](sca::parser::diagnostic&& d) {
        diagnostics += d.message();
        diagnostics += "\f\f";
    };

    /// Make sure to use the right mode.
    switch (mode) {
        case sca_mode::normal:
            if (!p.parse_classes(parser_inputs[0])) NAPI_RAISE(diagnostics.c_str());
            if (!p.parse_rules(parser_inputs[1])) NAPI_RAISE(diagnostics.c_str());
            break;
        case sca_mode::zompist:
            p.sca2_mode = true;
            parser_inputs[0] += parser_inputs[1];
            if (!p.zompist_parse(parser_inputs[0])) NAPI_RAISE(diagnostics.c_str());
            break;
        case sca_mode::$$count: UNREACHABLE();
    }

    /// Parse the words.
    auto [words, success] = p.parse_words(parser_inputs[2]);
    if (!success) NAPI_RAISE(diagnostics.c_str());

    /// Apply the words. Make sure to check for exceptions.
    std::vector<sca::word> result;
    try {
        sca::applier a{p, true};
        result = a.apply(words);
    } catch (const sca::compiler::error& e) {
        NAPI_RAISE(fmt::format("Compiler Error: {}", e.what()).c_str());
    }

    /// Join the resulting words together.
    std::string joined;
    for (const auto& item : result) {
        joined += sca::to_utf8(item.data);
        joined += "\n";
    }

    /// Convert them to a javascript string.
    napi_value js_string;
    NAPI_CALL(napi_create_string_utf8(env, joined.c_str(), joined.size(), &js_string));

    /// Create an array to hold the words and diagnostics, and convert the latter to a js string.
    napi_value ret;
    NAPI_CALL(napi_create_array_with_length(env, 2, &ret));

    napi_value diags;
    NAPI_CALL(napi_create_string_utf8(env, diagnostics.c_str(), diagnostics.size(), &diags));

    /// Add the results to the array and return it.
    NAPI_CALL(napi_set_element(env, ret, 0, js_string));
    NAPI_CALL(napi_set_element(env, ret, 1, diags));
    return ret;
}
NAPI_CALLBACK_END()

NAPI_CALLBACK(tokenise_text, 1) {
    /// The argument must be a string.
    std::u32string text;
    NAPI_ASSERT_TYPE(argv[0], napi_string);
    NAPI_GET_U32STRING(text, argv[0]);

    /// Set up the parser and ignore diagnostics.
    sca::parser p;
    p.use_term_colours   = false;
    p.warnings           = sca::parser::warn_none;
    p.diagnostic_handler = [&](sca::parser::diagnostic&& d) {};

    /// Tokenise the text.
    p.init(text);
    auto toks = p.tokens();

    /// Create a js array containing the token ranges.
    napi_value ret;
    NAPI_CALL(napi_create_array_with_length(env, toks.size(), &ret));

    /// Add the tokens.
    for (u64 i = 0; i < toks.size(); i++) {
        /// Create an empty array of size 4.
        napi_value range, start_line, start_col, end_line, end_col;
        NAPI_CALL(napi_create_array_with_length(env, 4, &range));

        /// Convert the line and col poss to js numbers.
        /// CodeMirror expects 0-based indices.
        NAPI_CALL(napi_create_int32(env, toks[i].pos.start.line - 1, &start_line));
        NAPI_CALL(napi_create_int32(env, toks[i].pos.start.col - 1, &start_col));
        NAPI_CALL(napi_create_int32(env, toks[i].pos.end.line - 1, &end_line));
        NAPI_CALL(napi_create_int32(env, toks[i].pos.end.col - 1, &end_col));

        /// Store the numbers in the array.
        NAPI_CALL(napi_set_element(env, range, 0, start_line));
        NAPI_CALL(napi_set_element(env, range, 1, start_col));
        NAPI_CALL(napi_set_element(env, range, 2, end_line));
        NAPI_CALL(napi_set_element(env, range, 3, end_col));

        /// Store the array in the array.
        NAPI_CALL(napi_set_element(env, ret, static_cast<i32>(i), range));
    }

    return ret;
}
NAPI_CALLBACK_END()

NAPI_MODULE_INIT() {
    /// Enable colours for assertion errors.
    assertion_error::use_colour = true;

    /// Create the module.
    napi_value module;
    NAPI_CALL(napi_create_object(env, &module));

    /// Export our functions.
    NAPI_EXPORT(parse_and_apply);
    NAPI_EXPORT(tokenise_text);

    return module;
}
