if (!window.electron) throw new Error('This file may only be loaded in Electron-mode')

window.__ipc = {}
if (window.electron) {
    let {ipcRenderer} = require('electron')
    __ipc = ipcRenderer;
}
const ipcRenderer = window.__ipc

let titlebar = document.getElementById('title-bar')
if (window.electron) {

    ///
    /// Title bar
    ///
    let close_button = document.getElementById('close-button')
    let maximise_restore_button = document.getElementById('maximise-restore-button')
    let minimise_button = document.getElementById('minimise-button')

    close_button.addEventListener('click', () => {
        ipcRenderer.send('close-window')
    })
    minimise_button.addEventListener('click', () => {
        ipcRenderer.send('minimise')
    })
    maximise_restore_button.addEventListener('click', () => {
        ipcRenderer.send('maximise-restore')
    })

    ///
    /// Shortcuts
    ///
    const min_zoom = -3
    const max_zoom = 10

    ipcRenderer.on('zoom-in', () => {
        /// Get the current font size.
        let cs = getComputedStyle(root)
        let newsize = Number.parseInt(cs.getPropertyValue('--zoom-factor')) + 1

        /// We don't want to zoom in more than that.
        if (newsize > max_zoom) return

        /// Zoom in.
        root.style.setProperty('--zoom-factor', String(newsize))
    })

    ipcRenderer.on('zoom-out', () => {
        /// Get the current font size.
        let cs = getComputedStyle(root)
        let newsize = Number.parseInt(cs.getPropertyValue('--zoom-factor')) - 1

        /// We don't want to zoom out more than that.
        if (newsize < min_zoom) return

        /// Zoom in.
        root.style.setProperty('--zoom-factor', String(newsize))
    })

    ipcRenderer.on('zoom-reset', () => void root.style.removeProperty('--zoom-factor'))

    let max_restore = document.getElementById('maximise-restore-button')
    ipcRenderer.on('restored', () => {
        titlebar.classList.remove('maximised')
        max_restore.title = 'Maximise'
    })
    ipcRenderer.on('maximised', () => {
        titlebar.classList.add('maximised')
        max_restore.title = 'Restore'
    })
}