# 'Inspired' by https://nodejs.github.io/node-addon-examples/build-tools/cmake-js/
cmake_minimum_required(VERSION 3.21)
cmake_policy(SET CMP0042 NEW)
set (CMAKE_CXX_STANDARD 23)

project (sca)
file(GLOB SOURCE_FILES "src/electron/*.cc")

link_directories("$ENV{SCA_PROJ_ROOT_DIR}/out-windows/"
        "$ENV{SCA_PROJ_ROOT_DIR}/out-windows/3rdparty/fmt/"
        "$ENV{SCA_PROJ_ROOT_DIR}/frontend/deps/windows/"
        "$ENV{HOME}/libs/mingw/node-v16.13.2-win-x64"
        )
add_compile_options(-fdiagnostics-color=always)
add_compile_options(-fexceptions)
add_link_options(-fexceptions -Wl,--allow-shlib-undefined -Wl,--export-all-symbols)

add_library(${PROJECT_NAME} SHARED ${SOURCE_FILES})
set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "" SUFFIX ".node")
target_compile_options(${PROJECT_NAME} PRIVATE
        -Wall -Wextra -Wundef -Werror=return-type
        -Wno-unused-function -Wno-unused-parameter -Wno-unused-variable
        -Wno-empty-body -Wno-nonnull)

target_link_libraries(${PROJECT_NAME} sca++ fmt icuuc "$ENV{HOME}/libs/mingw/node-v16.13.2-win-x64/libnode.delayed.a")

# define NAPI_VERSION
add_definitions(-DNAPI_VERSION=8)