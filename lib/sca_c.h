#ifndef SCA_C_H
#define SCA_C_H

#include <stddef.h>
/**
 * C-compatible interface to SCA++
 */

#ifdef __cplusplus
extern "C" {
#endif

/** Container for the results of sca_parse_and_apply */
typedef struct sca_parse_apply_res {
    const char* result; size_t result_len;
    const char* diags;  size_t diags_len;
} sca_parse_apply_res_t;
/** Dialect used for parsing the rules and classes */
typedef enum sca_dialect {
    SCA_DIALECT_NORMAL = 0, /**< Normal SCA++ behaviour */
    SCA_DIALECT_ZOMPIST = 1, /**< SCA2-like Zompist behaviour */
} sca_dialect_t;

/** Result codes returned by the C API */
typedef enum sca_result_code {
    SCA_RESULT_OK = 0,
    SCA_RESULT_ERR = 1,
} sca_result_code_t;
/**
 * Run SCA
 *
 * The string parameters dont need to be NUL terminated
 * \param[in] classes,classes_len a (UTF-8) string containing the class
 *     definitions and its length
 * \param[in] rules,rules_len a (UTF-8) string containing the rules definition
 *     and its length
 * \param[in] words,words_len a (UTF-8) string containing the words on which
 *     to apply the sound changes and its length
 * \param[in] dialect the dialect in which to interpret the rules and classes
 * \param[out] result struct filled by the function
 * \return SCA_RESULT_ERR if an error occurs, SCA_RESULT_OK if it succeeds
 */
sca_result_code_t sca_parse_and_apply(
        const char* classes, size_t classes_len,
        const char* rules,   size_t rules_len,
        const char* words,   size_t words_len,
        sca_dialect_t dialect,
        sca_parse_apply_res_t* res);
#ifdef __cplusplus
}
#endif

#endif // SCA_C_H
