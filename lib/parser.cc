#include "parser.hh"

#include "../3rdparty/fmt/include/fmt/xchar.h"
#include "unicode.hh"

#include <algorithm>
#include <numeric>
#include <random>

namespace sca {
/// ====================================================================== ///
///  Macros                                                                ///
/// ====================================================================== ///
#define ASSERT_IS_TYPE(tok, t) ASSERT((tok).type == token_type::t, \
    "Expected {}, but was {}", token::str(token_type::t), (tok).str())

/// ====================================================================== ///
///  Static Variables                                                      ///
/// ====================================================================== ///

bool parser::default_use_term_colours = true;

/// ====================================================================== ///
///  `word` Implementation                                                 ///
/// ====================================================================== ///

auto word::reverse() const -> std::u32string {
    std::u32string result;
    for (auto it = chars.rbegin(); it != chars.rend(); ++it) result.append(data.data() + it->pos, it->len);
    return result;
}

/// ====================================================================== ///
///  Miscellaneous                                                         ///
/// ====================================================================== ///

std::string enum_name(parser::rule_type t) {
    switch (t) {
        case parser::rule_type::substitution: return "SUBSTITUTION RULE";
        case parser::rule_type::epenthesis: return "EPENTHESIS RULE";
        case parser::rule_type::deletion: return "DELETION RULE";
        case parser::rule_type::metathesis: return "METATHESIS RULE";
        case parser::rule_type::reduplication: return "REDUPLICATION RULE";
    }
    ASSERT(false, "Invalid rule type '{}'", u32(t));
}

std::string enum_name(parser::element_type t) {
    switch (t) {
        case parser::element_type::empty: return "empty";
        case parser::element_type::wildcard: return "wildcard";
        case parser::element_type::word_boundary: return "word boundary";
        case parser::element_type::syllable_boundary: return "syllable boundary";
        case parser::element_type::text: return "text";
        case parser::element_type::list: return "list";
        case parser::element_type::chain: return "chain";
        case parser::element_type::cclass: return "cclass";
        case parser::element_type::negative: return "negative";
        case parser::element_type::optional: return "optional";
    }
    ASSERT(false, "Invalid element type '{}'", u32(t));
}

std::u32string indent(u64 depth) {
    std::u32string s;
    for (u64 i = 0; i < depth; i++) s += U"  ";
    return s;
}

static std::u32string format_list_like(const parser::element& el, u64 depth) {
    std::u32string s;
    for (const auto& item : el.els) {
        s += U"\n";
        s += item.str(depth + 1);
    }
    s += U")";
    return s;
}

template <typename what>
bool set_if_success(what& w, parser::parse_result<what>&& res) {
    if (res.success) w = std::move(res.value);
    return res.success;
}

template <typename what>
bool append_if_success(std::vector<what>& w, parser::parse_result<what>&& res) {
    if (res.success) w.push_back(std::move(res.value));
    return res.success;
}

static const char* set_operator_to_str(parser::token_type t) {
    switch (t) {
        case parser::token_type::ampersand: return "Intersection operator \"&\"";
        case parser::token_type::plus: return "Concatenation operator \"+\"";
        case parser::token_type::tilde: return "Difference operator \"~\"";
        case parser::token_type::vbar: return "Union operator \"|\"";
        default: ASSERT(false, "Invalid operator type '{}'", u32(t));
    }
};

/// ====================================================================== ///
///  `parser` Implementation -- Miscellaneous                              ///
/// ====================================================================== ///

void parser::init_lexer(const std::u32string_view& _input, u32 line_pos, u32 col_pos) {
    input_text        = _input;
    where             = input_text.c_str();
    end               = where + input_text.size();
    lastc             = U' ';
    at_end            = false;
    loc.line          = line_pos;
    loc.col           = col_pos; /// Default: BEFORE the first character in the first line.
    last_line_end_col = loc.col;
    has_error         = false;

    /// Read the first character.
    next_char();
}

void parser::init(const std::u32string_view& _input, u32 line_pos, u32 col_pos) {
    init_lexer(_input, line_pos, col_pos);

    /// Read the first token.
    next_token();
}

/// Default error handler.
void parser::default_diagnostic_handler(diagnostic&& d) {
    if (d.p->diagnostics) std::cerr << d.message();
}

/// Convert a class entry into a string representation.
[[nodiscard]] auto parser::class_to_str(const std::u32string& name, const element& el) -> std::u32string {
    return std::u32string{fmt::format(U"(class :{} {}", name, format_list_like(el, 0))};
}

bool parser::seterr_with_note(range err_loc, std::string err_msg, range note_loc, std::string&& note_msg) {
    if (!has_error) {
        error(err_loc, "{}", err_msg);
        note(note_loc, "{}", note_msg);
        has_error = true;
    }
    return false;
}

/// ====================================================================== ///
///  `parser::diagnostic` Implementation                                   ///
/// ====================================================================== ///

static const char* diagnostic_title(parser::diagnostic_level level) {
    switch (level) {
        case parser::diagnostic_level::raw: return "Message";
        case parser::diagnostic_level::note: return "Note";
        case parser::diagnostic_level::warning: return "Warning";
        case parser::diagnostic_level::error: return "Error";
    }
    return "Message";
}

static const char* diagnostic_colour_str(parser::diagnostic_level level) {
    switch (level) {
        case parser::diagnostic_level::raw: return "";
        case parser::diagnostic_level::note: return "\033[1;32m";
        case parser::diagnostic_level::warning: return "\033[1;35m";
        case parser::diagnostic_level::error: return "\033[1;31m";
    }
    return "";
}

static const char32_t* diagnostic_colour_str_u32(parser::diagnostic_level level) {
    switch (level) {
        case parser::diagnostic_level::raw: return U"";
        case parser::diagnostic_level::note: return U"\033[32m";
        case parser::diagnostic_level::warning: return U"\033[35m";
        case parser::diagnostic_level::error: return U"\033[31m";
    }
    return U"";
}

/// Warning: for the sake of your sanity, do not attempt to understand the intricacies of
/// this function. It was devised as much through trial and error as it was through planning.
auto parser::diagnostic::line() const -> std::string {
    /// The line and col must be > 0.
    if (!pos.start.line || !pos.start.col || !pos.end.line || !pos.end.col) return "";

    /// Ensure that pos.start comes first.
    if (pos.end.line < pos.start.line) std::swap(pos.start, pos.end);

    /// Determine whether to use colours.
    const bool use_colour = p->use_term_colours && level != diagnostic_level::raw;

    /// Seek to the line.
    u32             line = 1;
    u32             col  = 1;
    const char32_t* curr = p->input_text.c_str();

    /// The check for line breaks is taken from next_char().
    while (line < pos.start.line && curr < p->end - 1) {
        char32_t c = *curr++;

        /// Handle platform-dependent newlines correctly.
        /// Treat CRLF, and LFCR as one linebreak, but CRCR, LFLF as two.
        if (c == U'\n' || c == U'\r') {
            line++;
            if (char32_t c2 = *curr; (c2 == U'\n' || c2 == U'\r') && c != c2) curr++;
        }
    }

    /// It may be the case that the input either does not contain the line or it's the last line and it's empty.
    if (line < pos.start.line || curr >= p->end) return "";

    /// Otherwise, the input contains the line, and there are still characters left.
    const char32_t* const line_beginning_pos = curr;

    /// We next need to seek to the right column.
    /// Make sure that the col number doesn't put us past the end of the line/string.
    bool col_start_is_eol = false;
    while (col < pos.start.col) {
        if (curr >= p->end || *curr == U'\n' || *curr == U'\r') break;
        curr++;
        col++;
    }

    /// If we get here, curr points at the start column.
    const char32_t* const col_start_pos = curr;

    /// This is for when the token whose position we want to highlight is END OF LINE.
    if (curr >= p->end || *curr == U'\n' || *curr == U'\r') col_start_is_eol = true;

    /// Next, we need to find the end column.
    /// Make sure that the col number doesn't put us past the end of the line/string.
    /// If the start and end poss are on different lines, just grab the entire line.
    if (pos.start.line == pos.end.line) {
        while (col < pos.end.col) {
            if (curr >= p->end || *curr == U'\n' || *curr == U'\r') break;
            curr++;
            col++;
        }
    } else
        while (curr < p->end && *curr != U'\n' && *curr != U'\r') curr++;

    /// If we get here, curr points at the end column.
    const char32_t* const col_end_pos = curr;

    /// Find the end of the line.
    while (curr < p->end && *curr != U'\n' && *curr != U'\r') curr++;
    const char32_t* const line_end_pos = curr;

    /// Copy the line into a string.
    std::u32string line_str{U' '};
    line_str += to_utf32(std::to_string(pos.start.line));
    line_str += U" | ";
    line_str.append(line_beginning_pos, u64(col_start_pos - line_beginning_pos));

    /// Highlight the part between the start and end cols.
    /// If the start column is at the end of the line (i.e. if this is an EOL token),
    /// then we've already appended everything there was to append.
    if (!col_start_is_eol) {
        if (use_colour) line_str += diagnostic_colour_str_u32(level);
        line_str.append(col_start_pos, u64(col_end_pos - col_start_pos) + 1);
        if (use_colour) line_str += U"\033[m";

        /// Append the rest of the line
        line_str.append(col_end_pos + 1, col_end_pos + 1 > line_end_pos ? 0 : u64(line_end_pos - col_end_pos) - 1);
    }

    /// Add enough spaces to line up with the start of the line.
    line_str += U"\n ";
    for (i64 i = 0; i < number_width(pos.start.line); i++) line_str += U' ';
    line_str += U" | ";

    /// Append spaces to line up with the start column. Here, we need to keep in mind that
    /// certain characters in the line may be diacritics and thus have no width.
    i64 i = 0;
    // auto p = u_getUnicodeProperties(line_beginning_pos[i]);

    for (; i < pos.start.col - 1 /** Columns start at 1. **/; i++)
        if (!unicode::is_mark(line_beginning_pos[i])) line_str += U' ';

    /// Underline the everything between the start and end cols.
    /// Again, be wary of diacritics.
    if (use_colour) line_str += diagnostic_colour_str_u32(level);
    line_str += U'^';
    for (i++; i < pos.end.col; i++)
        if (!unicode::is_mark(line_beginning_pos[i])) line_str += U'~';

    /// Append another line break, and we're finally done.
    if (use_colour) line_str += U"\033[m\n";
    else line_str += U'\n';
    return sca::to_utf8(line_str);
}

/// Generate a properly formatted diagnostic message.
auto parser::diagnostic::message() const -> std::string {
    /// Make sure we don't use colours if we're not supposed to.
    return p->use_term_colours && level != diagnostic_level::raw
               ? fmt::format("{} {}{}: {}{}{}\n{}", pos.start, diagnostic_colour_str(level),
                   diagnostic_title(level), "\033[m\033[1;38m", msg, "\033[m", line())
               : fmt::format("{} {}: {}\n{}", pos.start, diagnostic_title(level), msg, line());
}

/// ====================================================================== ///
///  `parser::token` Implementation                                        ///
/// ====================================================================== ///

///
/// Token predicates used by the parser.
///

bool parser::token::boundary_char() const {
    return type == token_type::hash || type == token_type::dollar;
}

bool parser::token::can_be_start_of_input_el() const {
    return can_be_start_of_simple_el() || type == token_type::wildcard;
}

bool parser::token::can_be_start_of_decorated_el() const {
    return can_be_start_of_input_el() || boundary_char() || type == token_type::lparen || type == token_type::tilde;
}

bool parser::token::can_be_start_of_output_el() const {
    return can_be_start_of_simple_el() || type == token_type::percentage;
}

bool parser::token::can_be_start_of_percent_el() const {
    return type == token_type::text || type == token_type::lbrace;
}

bool parser::token::can_be_start_of_percent_els() const {
    return can_be_start_of_percent_el() || type == token_type::percentage;
}

bool parser::token::can_be_start_of_simple_el() const {
    return type == token_type::text || type == token_type::class_name || type == token_type::lbrace;
}

bool parser::token::can_be_start_of_z_char_or_cat() const {
    return type == token_type::text || type == token_type::class_name || type == token_type::lbrack;
}

bool parser::token::can_be_start_of_z_context_element() const {
    return can_be_start_of_z_char_or_cat() || type == token_type::z_ellipsis || type == token_type::hash;
}

bool parser::token::can_be_start_of_z_decorated_context_element() const {
    return can_be_start_of_z_context_element() || type == token_type::lparen;
}

bool parser::token::can_be_start_of_z_ctx_cat_lit_mem() const {
    return type == token_type::text || type == token_type::class_name || type == token_type::hash;
}

bool parser::token::eol() const {
    return type == token_type::newline || type == token_type::end_of_string;
}

bool parser::token::set_operator() const {
    return type == token_type::ampersand
           || type == token_type::plus
           || type == token_type::tilde
           || type == token_type::vbar;
}

/// The token names returned by this function may end up in error messages,
/// which is why they have more descriptive names and are in all-caps to
/// stand out more.
auto parser::token::str(token_type type) -> std::string {
    switch (type) {
        case token_type::end_of_string: return "END OF STRING";
        case token_type::text: return "TEXT";
        case token_type::class_name: return "CLASS NAME";
        case token_type::separator: return "SEPARATOR";
        case token_type::arrow: return "ARROW SEPARATOR";
        case token_type::lbrace: return "LEFT BRACE";
        case token_type::rbrace: return "RIGHT BRACE";
        case token_type::lbrack: return "LEFT SQUARE BRACKET";
        case token_type::rbrack: return "RIGHT SQUARE BRACKET";
        case token_type::lparen: return "LEFT PARENTHESIS";
        case token_type::rparen: return "RIGHT PARENTHESIS";
        case token_type::hash: return "HASH SIGN";
        case token_type::dollar: return "DOLLAR SIGN";
        case token_type::ampersand: return "AMPERSAND";
        case token_type::wildcard: return "WILDCARD";
        case token_type::uscore: return "UNDERSCORE";
        case token_type::assign: return "EQUALS SIGN";
        case token_type::tilde: return "TILDE";
        case token_type::comma: return "COMMA";
        case token_type::vbar: return "VERTICAL BAR";
        case token_type::newline: return "END OF LINE";
        case token_type::plus: return "PLUS";
        case token_type::percentage: return "PERCENTAGE";
        case token_type::z_arrow_separator: return "SCA2 ALTERNATIVE SEPARATOR";
        case token_type::z_comment: return "MINUS";
        case token_type::z_dbl_backslash: return "SCA2 DOUBLE BACKSLASH";
        case token_type::z_superscript_2: return "SCA2 SUPERSCRIPT 2";
        case token_type::z_ellipsis: return "SCA2 ELLIPSIS (TRIPLE DOTS)";
        case token_type::invalid: break;
    }
    return "INVALID TOKEN";
}

auto parser::token::str() const -> std::string {
    switch (type) {
        case token_type::text: return "TEXT (" + to_utf8(text.data) + ")";
        case token_type::class_name: return "CLASS NAME (" + to_utf8(text.data) + ")";
        case token_type::plus: return number < 2 ? "PLUS" : "PLUS (reps: " + std::to_string(number) + ")";
        case token_type::percentage: return "PERCENTAGE (" + std::to_string(number) + "%)";
        default: return str(type);
    }
}

auto parser::tokens() -> std::vector<token> try {
    std::vector<token> toks;
    while (tok.type != token_type::end_of_string) {
        toks.push_back(tok);
        next_token();
    }
    return toks;
} catch (std::exception& e) {
    error({loc, loc}, "{}", e.what());
    return {};
}

auto parser::tokens(const std::u32string& input) -> std::vector<token> {
    parser p{input};
    return p.tokens();
}

/// ====================================================================== ///
///  `parser` Implementation -- Lexer                                      ///
/// ====================================================================== ///

///
/// Lexer predicates
///

static constexpr bool is_number(char32_t c) { return c >= U'0' && c <= U'9'; }

static constexpr bool is_special_character(char32_t c, bool parsing_zompist) {
    switch (c) {
        case U'/':
        case U'>':
        case U'{':
        case U'}':
        case U'[':
        case U']':
        case U'(':
        case U')':
        case U'#':
        case U'$':
        case U'%':
        case U'*':
        case U'&':
        case U'+':
        case U'_':
        case U'=':
        case U'~':
        case U',':
            return true;
        default:
            if (parsing_zompist && (c == U'-' || c == U'.' || c == U'…')) return true;
            if (unicode::is_control(c)) return true;
            return false;
    }
}

///
/// Basic operations
///

void parser::restore(const lexer_pos& p) {
    where             = p.where;
    lastc             = p.lastc;
    at_end            = p.at_end;
    last_text         = p.last_text;
    loc               = p.loc;
    last_line_end_col = p.last_line_end_col;
}

void parser::rewind() {
    rewind_lexer();
    next_token();
}

void parser::rewind_lexer() {
    where             = input_text.c_str();
    end               = where + input_text.size();
    lastc             = U' ';
    at_end            = false;
    loc.line          = 1;
    loc.col           = 0;
    last_line_end_col = loc.col;
    has_error         = false;

    next_char();
}

auto parser::save() -> lexer_pos {
    return {
        .where             = where,
        .lastc             = lastc,
        .at_end            = at_end,
        .last_text         = last_text,
        .loc               = loc,
        .last_line_end_col = last_line_end_col,
    };
}

void parser::skip_line() {
    while (!at_end && lastc != '\n') next_char();
    next_char();
}

void parser::discard() {
    next_token();
}

[[nodiscard]] bool parser::advance() {
    next_token();
    return tok.type != token_type::end_of_string;
}

[[nodiscard]] bool parser::consume(token_type t) {
    if (tok.type == t) {
        discard();
        return true;
    }
    return false;
}

[[nodiscard]] bool parser::consume_and_advance(token_type t) {
    return tok.type == t && advance();
}

///
/// Functions that produce tokens: `next_*`
///

void parser::next_char() {
    /// Keep returning END_OF_FILE once the end of the string has been reached.
    if (at_end || where >= end) {
        at_end = true;
        lastc  = char32_t(EOF);
        return;
    }
    /// Get the next character and increment the position accordingly.
    char32_t c = *where++;
    loc.col++;
    if (where >= end) {
        lastc = c;
        if (lastc == '\r') lastc = '\n';
        return;
    }
    /// Handle platform-dependent newlines correctly.
    if (c == '\n' || c == '\r') {
        last_line_end_col = loc.col;
        loc.col           = 1;
        loc.line++;
        lastc = '\n';
        /// Treat CRLF, LFCR as one linebreak but CRCR, LFLF as two.
        if (char32_t c2 = *where; (c2 == '\n' || c2 == '\r') && c != c2) where++;
        return;
    }
    /// Set the character and return if it isn't a linebreak.
    lastc = c;
}

void parser::next_number(u64 num, const char32_t* start_pos) {
    u64 old_num{};
    for (const char32_t* tok_pos = start_pos;; tok_pos++) {
        if (tok_pos >= where - 1) break;
        old_num = num;
        num *= 10;
        if (num < old_num) {
            tok.pos.end = {u32(loc.line), u32(loc.col)};
            error(tok.pos, "Number too large");
            tok.type = token_type::invalid;
            return;
        }
        old_num = num;
        num += *tok_pos - U'0';
        if (num < old_num) {
            tok.pos.end = {u32(loc.line), u32(loc.col)};
            error(tok.pos, "Number too large");
            tok.type = token_type::invalid;
            return;
        }
    }
    tok.pos.end = {u32(loc.line), u32(loc.col)};
    next_char(); /// Yeet '%'
    tok.type   = token_type::percentage;
    tok.number = num;
}

void parser::next_text() {
    auto collect_diacritics = [&] {
        while (!at_end && unicode::is_diacritic(lastc)) {
            last_text += lastc;
            bool tie = unicode::is_tie(lastc);
            next_char();

            /// If this diacritic is a tie bar or similar, append
            /// the next letter no matter what.
            while (!at_end
                   && tie
                   && !is_special_character(lastc, sca2_mode)
                   && !unicode::is_whitespace(lastc)) {
                last_text += lastc;
                tie = unicode::is_tie(lastc);
                next_char();
            }
        }
    };

    loc = {loc.line, loc.col};
    last_text.clear();
    if (unicode::is_whitespace(lastc)) {
        last_text = lastc;
        next_char();
        return;
    }

    /// Collect all the diacritics before this character.
    collect_diacritics();

    if (at_end || is_special_character(lastc, sca2_mode)) {
        if (at_end && last_text.empty()) return;
        std::string warnmsg = fmt::format("Stray diacritic{}", last_text.empty() ? "s" : "");
        warnmsg += fmt::format(" '{}' (codepoints: ", to_utf8(last_text));
        bool first = true;
        for (const auto c : last_text) {
            if (!first) warnmsg += ", ";
            else first = false;
            warnmsg += fmt::format("U+{:X}", u64(c));
        }
        warnmsg += ")";
        warning(warn_stray_diacritic, {loc, loc}, "{}", warnmsg);
        return;
    }

    /// Append the character.
    last_text += lastc;
    next_char();

    /// Collect all the diacritics after this character.
    collect_diacritics();
}

void parser::next_token() {
    tok.type     = token_type::invalid;
    tok.text_pos = where - 1;

    /// Skip whitespace when not in SCA2 mode.
    /// Take care not to skip LF if we're parsing rules, since
    /// in that case, LF is a valid token.
    while (!sca2_mode && unicode::is_whitespace(lastc) && !at_end) {
        if (lastc == U'\n') {
            if (parsing_rule) break;
            else loc.col--;
        }
        next_char();
    }

    tok.pos.start = tok.pos.end = {loc.line, loc.col};

    /// Keep returning eof once eof has been reached.
    if (at_end) {
        tok.type = token_type::end_of_string;
        return;
    }

    /// The token is a single char.
    switch (lastc) {
    done:
        next_char();
        return;

        /// Single-char tokens
        case U'/': tok.type = token_type::separator; goto done;
        case U'>': tok.type = token_type::arrow; goto done;
        case U'{': tok.type = token_type::lbrace; goto done;
        case U'}': tok.type = token_type::rbrace; goto done;
        case U'[': tok.type = token_type::lbrack; goto done;
        case U']': tok.type = token_type::rbrack; goto done;
        case U'(': tok.type = token_type::lparen; goto done;
        case U')': tok.type = token_type::rparen; goto done;
        case U'#': tok.type = token_type::hash; goto done;
        case U'$': tok.type = token_type::dollar; goto done;
        case U'&': tok.type = token_type::ampersand; goto done;
        case U'=': tok.type = token_type::assign; goto done;
        case U'~': tok.type = token_type::tilde; goto done;
        case U',': tok.type = token_type::comma; goto done;
        case U'*': tok.type = token_type::wildcard; goto done;
        case U'|': tok.type = token_type::vbar; goto done;

        /// SCA2 tokens
        case U'²':
            if (sca2_mode) {
                tok.type = token_type::z_superscript_2;
                goto done;
            }
            break;
        case U'…':
            if (sca2_mode) {
                tok.type = token_type::z_ellipsis;
                goto done;
            }
            break;
        case U'.':
            if (sca2_mode) {
                auto s = save();
                next_char();
                if (lastc == U'.') {
                    next_char();
                    if (lastc == U'.') {
                        tok.type = token_type::z_ellipsis;
                        goto done;
                    }
                }
                restore(s);
            }
            break;
        case U'-':
            if (sca2_mode) {
                tok.type = token_type::z_comment;
                goto done;
            }
            break;
        case U'→':
            if (sca2_mode) {
                tok.type = token_type::z_arrow_separator;
                goto done;
            }
            break;
        case U'\\':
            if (sca2_mode) {
                auto s = save();
                next_char();
                if (lastc == U'\\') {
                    tok.type = token_type::z_dbl_backslash;
                    goto done;
                } else restore(s);
            }
            break;

        /// Complicated stuff
        case U'\n':
            next_char();
            tok.type = token_type::newline;
            loc.col--;
            /// The position of a LF is conceptually supposed to be after the last column of the previous line.
            if (!at_end) {
                tok.pos.start.line--;
                tok.pos.start.col = last_line_end_col;
                tok.pos.end       = tok.pos.start;
            }
            return;
        case U'_':
            do next_char();
            while (lastc == U'_');
            tok.type = token_type::uscore;
            return;
        case U'+':
            tok.number = 0;
            do {
                next_char();
                tok.number++;
            } while (lastc == U'+');
            tok.type = token_type::plus;
            return;
        case U'%': {
            /// A '%' without a number is equal to 100% so long as the next token is \"{\".
            auto here = tok.pos;
            next_char();
            if (lastc != U'{') {
                tok.type = token_type::invalid;
                seterr(here, "A percentage without a number is only allowed before \"{{\"");
            } else {
                tok.number = 100;
                tok.type   = token_type::percentage;
            }
            return;
        }
    }

    /// The token may be a percentage.
    /// To determine whether it is, look ahead until we find either
    /// a per cent sign, or something that is not a number.
    if (is_number(lastc)) {
        u64  num = lastc - U'0';
        auto s   = save();
        do next_char();
        while (is_number(lastc));

        /// Rollback and parse it as text instead if we're not looking at a "%".
        if (lastc != U'%') restore(s);
        else {
            next_number(num, s.where);
            return;
        }
    }

    /// Otherwise, the token is text.
    /// In SCA2 mode, we don't care about diacritics, and each
    /// text token is exactly one character
    if (sca2_mode) {
        tok.text = std::u32string{lastc};
        next_char();
    } else {
        tok.text = word{};
        do {
            next_text();
            tok.text += last_text;
        } while (!at_end && !is_special_character(lastc, sca2_mode) && !unicode::is_whitespace(lastc));
    }

    /// Class names are a separate token type.
    if (classes.contains(tok.text.data)) tok.type = token_type::class_name;
    else tok.type = token_type::text;

    /// Fix the end position.
    if (loc.line > tok.pos.start.line) tok.pos.end = {tok.pos.start.line, last_line_end_col - 1};
    else tok.pos.end = {u32(loc.line), u32(loc.col) - 1 * !at_end};
}

/// ====================================================================== ///
///  `parser::element` Implementation                                      ///
/// ====================================================================== ///

parser::element::element(element_type t) : type(t) {}
parser::element::element(element_type t, range r) : type(t), pos(r) {}

auto parser::element::str(u64 depth) const -> std::u32string {
    auto perc = [this]() -> std::u32string {
        if (percentage == 0) return U"";
        return fmt::format(U" :{}%", percentage);
    };

    switch (type) {
        case element_type::empty: return U"";
        case element_type::text: return fmt::format(U"{}(text{} {})", indent(depth), perc(), text.data);
        case element_type::list: return fmt::format(U"{}(list{}", indent(depth), format_list_like(*this, depth));
        case element_type::chain: return fmt::format(U"{}(chain{}{}", indent(depth), perc(), format_list_like(*this, depth));
        case element_type::cclass: return fmt::format(U"{}(class{}{}", indent(depth), perc(), format_list_like(*this, depth));
        case element_type::wildcard: return fmt::format(U"{}(wildcard)", indent(depth));
        case element_type::word_boundary: return fmt::format(U"{}(word-boundary)", indent(depth));
        case element_type::syllable_boundary: return fmt::format(U"{}(syllable-boundary)", indent(depth));
        case element_type::negative:
            return els.empty()
                       ? fmt::format(U"{}(negative <error>)", indent(depth))
                       : fmt::format(U"{}negative\n{})", indent(depth), els[0].str(depth + 1));
        case element_type::optional:
            return els.empty()
                       ? fmt::format(U"{}(optional <error>)", indent(depth))
                       : fmt::format(U"{}optional\n{})", indent(depth), els[0].str(depth + 1));
    }
    ASSERT(false, "Invalid element type '{}'", u32(type));
}

auto parser::element::construct_text() const -> std::u32string {
    /// Since we might end up calling this function a lot, we don't
    /// exactly want to reconstruct the rnd every time we call it.
    static auto r = [] {
        static std::mt19937                          construct_text_rnd{std::random_device()()};
        static std::uniform_real_distribution<float> construct_text_dist{0.f, 100.f};
        return construct_text_dist(construct_text_rnd);
    };

    /// We need to check a random number against the percentage
    /// of this element and this element only. For this reason, the
    /// recursion needs to be factored out into a separate function.
    static std::function<std::u32string(const parser::element&)> do_construct_text = [](const parser::element& el) -> std::u32string {
        /// If this is a class, it must have a percentage, and be non-empty.
        ASSERT(el.type != element_type::cclass || el.percentage != 0, "Class in output must have a percentage");

        /// If this element is TEXT, just return the text.
        if (el.type == element_type::text) return el.text.data;

        /// Otherwise, recurse.
        float f = r();
        for (const auto& e : el.els)
            if (e.percentage == 0 || f <= e.percentage)
                return do_construct_text(e);
        return U"";
    };

    /// Determine whether we're supposed to return something and return
    /// either nothing or the constructed text accordingly.
    return r() <= percentage ? do_construct_text(*this) : U"";
}

///
/// Element predicates
///

u64 parser::element::size() const {
    return percentage != 0 && !els.empty() ? 1 : els.size();
}

bool parser::element::operator==(const element& other) const {
    if (type != other.type) return false;
    switch (type) {
        case element_type::empty:
        case element_type::word_boundary:
        case element_type::syllable_boundary:
        case element_type::wildcard: return true;

        case element_type::text: return text == other.text;

        case element_type::list:
        case element_type::chain:
        case element_type::cclass:
        case element_type::negative:
        case element_type::optional:
            return els == other.els;
    }
    ASSERT(false, "Invalid element type '{}'", u32(type));
}

[[nodiscard]] bool parser::element::empty() const {
    switch (type) {
        case element_type::empty: return true;

        case element_type::wildcard:
        case element_type::word_boundary:
        case element_type::syllable_boundary:
        case element_type::text: return false;

        case element_type::list:
        case element_type::chain:
        case element_type::cclass:
            return els.empty() || std::all_of(els.begin(), els.end(), [](const auto& item) { return item.empty(); });

        case element_type::optional:
        case element_type::negative:
            return els.empty() || els[0].empty();
    }
    ASSERT(false, "Invalid element type '{}'", u32(type));
}

///
/// Functions that create elements
///

auto parser::element::from_curr_text_tok(parser& p) -> element {
    element el;
    el.type    = element_type::text;
    el.pos     = p.tok.pos;
    el.text    = std::move(p.tok.text);
    p.tok.text = word{};
    p.discard();
    return el;
}

auto parser::element::from_text(word&& txt, range r) -> element {
    element el;
    el.type = element_type::text;
    el.pos  = r;
    el.text = std::move(txt);
    return el;
}

[[nodiscard]] bool parser::element::apply_class_operation(parser& p, element&& rhs, token_type op) {
    /// Make sure this is a valid element
    if (type != parser::element_type::cclass) {
        p.seterr(pos, "Left-hand side of {} must be a class", set_operator_to_str(op));
        return false;
    }

    /// Text elements (rhs only) are fine, but we're converting them to classes for simplicity.
    /// Then, flatten this list and the subtrahend to ease comparison.
    if (rhs.type != parser::element_type::cclass && rhs.type != parser::element_type::text) {
        p.seterr(rhs.pos, "Right-hand side of {} must be a class or text", set_operator_to_str(op));
        return false;
    }

    /// Set the position of this element to encompass the entire operation.
    pos.end = rhs.pos.end;

    /// This may turn both lhs and rhs into text elements.
    flatten();
    rhs.flatten();

    /// Apply the operation.
    switch (op) {
        case token_type::ampersand: cl_intersection(std::move(rhs), std::addressof(p)); break;
        case token_type::plus: cl_catenate(std::move(rhs), std::addressof(p)); break;
        case token_type::tilde: cl_difference(std::move(rhs), std::addressof(p)); break;
        case token_type::vbar: cl_union(std::move(rhs), std::addressof(p)); break;
        default: ASSERT(false, "Invalid operation type \"{}\"", u32(op));
    }
    return true;
}

void parser::element::cl_cartesian_product(element&& rhs, parser* p) {
    /// If either element is text, then this does nothing.
    /// We still have to compute the product and create a class, but we should
    /// warn the user that they're making us do unnecessary work.
    if (type == element_type::text || rhs.type == element_type::text) {
        if (type == rhs.type) {
            text += rhs.text;
            *this = wrap<element_type::cclass>(std::move(*this));
        } else if (type == element_type::text) {
            for (auto& item : rhs.els) {
                item.text = text + item.text;
                item.pos  = pos;
            }
            rhs.pos.start = pos.start;
            *this         = std::move(rhs);
        } else {
            for (auto& item : els) {
                item.text += rhs.text;
                item.pos = pos;
            }
        }
        if (p) p->warning(warn_pointless_operation, pos, "Multiplication has no effect here. You can remove the \"*\"");
        return;
    }

    /// Otherwise, we need to merge the two elements.
    elements new_els;
    for (const auto& l : els)
        for (const auto& r : rhs.els) {
            element el;
            el.type = element_type::text;
            el.pos  = pos;
            el.text = l.text + r.text;
            new_els.push_back(std::move(el));
        }
    els = std::move(new_els);
}

void parser::element::cl_catenate(element&& rhs, parser* p) {
    /// If either element is text, either just concatenate them or
    /// append the text element to the first element of the non-text
    /// element.
    if (type == element_type::text || rhs.type == element_type::text) {
        if (type == rhs.type) {
            text += rhs.text;
            *this = wrap<element_type::cclass>(std::move(*this));
            if (p) p->warning(warn_pointless_operation, pos, "Concatenation has no effect");
        } else if (type == element_type::text) {
            ASSERT(!rhs.els.empty(), "Classes cannot be empty");
            rhs.els[0].text = text + rhs.els[0].text;
            *this           = std::move(rhs);
        } else {
            ASSERT(!els.empty(), "Classes cannot be empty");
            els[0].text += rhs.text;
        }
        return;
    }

    /// Concatenate elements so long as both classes still contain elements.
    u64 i = 0;
    for (; i < std::min(els.size(), rhs.els.size()); i++) {
        els[i].pos = pos;
        els[i].text += rhs.els[i].text;
    }

    /// If the rhs class contains more elements, append them.
    /// Otherwise, we're done
    if (rhs.els.size() > els.size()) els.insert(els.end(), rhs.els.begin() + i64(i), rhs.els.end());
}

void parser::element::cl_difference(element&& rhs, parser* p) {
    /// We might need this later.
    auto sz = els.size();

    /// Either element could be text.
    if (type == element_type::text) {
        if ((rhs.type == element_type::text && text == rhs.text) || contains(rhs.els, *this)) goto empty;
        *this = wrap<element_type::cclass>(std::move(*this));
        goto no_effect;
    } else if (rhs.type == element_type::text) {
        std::erase_if(els, [&](const element& el) { return el == rhs; });
        if (els.empty()) goto empty;
        else if (sz == els.size()) goto no_effect;
        return;
    }

    /// Otherwise, remove all elements from this class that are also in rhs.
    std::erase_if(els, [&](const element& el) { return contains(rhs.els, el); });

    /// Warn if this ends up removing all elements.
    if (els.empty()) {
    empty:
        if (p) p->warning(warn_empty, pos, "Difference operation results in an empty class");
        *this = {};
        return;
    }

    /// Warn if this ends up removing no elements at all.
    if (sz == els.size()) {
    no_effect:
        if (p) p->warning(warn_pointless_operation, pos, "Difference operation has no effect");
    }
}

void parser::element::cl_intersection(element&& rhs, parser* p) {
    /// We might need this later
    auto sz = els.size();

    /// If the lhs is a text element, then we only need to compare it
    /// with the rhs if it's also a text element, or check if the rhs
    /// contains this element if it's a class.
    if (type == element_type::text || rhs.type == element_type::text) {
        if (type == rhs.type) {
            if (text != rhs.text) goto empty;
            *this = wrap<element_type::cclass>(std::move(*this));
            goto no_effect;
        } else if (type == element_type::text) {
            if (contains(rhs.els, *this)) {
                /// If the rhs contains only one element, and that element
                /// is the same as us, then this has no effect.
                if (rhs.els.size() == 1) goto no_effect_pos;

                *this = wrap<element_type::cclass>(std::move(*this));
                return;
            }
            goto empty;
        } else if (rhs.type == element_type::text) {
            if (contains(els, rhs)) {
                /// If we contain only one element, and that element is the
                /// same as the element on the rhs, then this has no effect.
                if (els.size() == 1) goto no_effect_pos;

                rhs.pos = pos;
                *this   = wrap<element_type::cclass>(std::move(rhs));
                return;
            }
            goto empty;
        }
        return;
    }

    /// Otherwise, remove all elements in the rhs from this element.
    std::erase_if(els, [&](const element& el) { return !contains(rhs.els, el); });

    /// Warn if this results in an empty class.
    if (els.empty()) {
    empty:
        *this = {};
        if (p) p->warning(warn_empty, pos, "Intersection results in an empty class");
    }

    /// Warn if this has no effect.
    if (sz == els.size()) {
    no_effect_pos:
        pos.end = rhs.pos.end;
    no_effect:
        if (p) p->warning(warn_empty, pos, "Intersection has no effect");
    }
}

void parser::element::cl_union(element&& rhs, parser* p) {
    /// Either element can be text.
    if (type == element_type::text || rhs.type == element_type::text) {
        if (type == element_type::text && rhs.type == element_type::text) {
            /// If both elements are the same, then this has no effect.
            bool same = text == rhs.text;
            *this     = wrap<element_type::cclass>(std::move(*this));
            if (same) {
                if (p) p->warning(warn_pointless_operation, pos, "Union operation has non effect: left and right operand are the same");
            } else els.push_back(std::move(rhs));
        } else if (type == element_type::text) {
            /// If the rhs already contains this element, then we don't need to do much.
            if (contains(rhs.els, *this)) {
                if (p) p->warning(warn_pointless_operation, pos, "Union operation has non effect: right element already contains left element");
            } else rhs.els.insert(rhs.els.begin(), std::move(*this));

            rhs.pos = pos;
            *this   = std::move(rhs);
        } else {
            /// If we already contain the rhs, then this has no effect.
            if (contains(els, rhs)) {
                if (p) p->warning(warn_pointless_operation, pos, "Union operation has non effect: left element already contains right element");
            } else els.push_back(std::move(rhs));
        }
        return;
    }

    /// We'll need this in a moment to see if something changed.
    auto sz = std::max(els.size(), rhs.els.size());

    /// Otherwise, merge rhs into this element.
    for (const auto& el : rhs.els)
        if (!contains(els, el))
            els.push_back(el);

    /// Check if this actually did something.
    if (sz == els.size() && p) p->warning(warn_pointless_operation, pos, "Union operation has no effect: left and right operand are the same");
}

///
/// Functions that transform elements
///

/// Warning: This function is extremely volatile and has
/// caused the implementer great pain. Do not touch.
void parser::element::flatten() {
    switch (type) {
        case element_type::empty:
        case element_type::wildcard:
        case element_type::word_boundary:
        case element_type::syllable_boundary:
        case element_type::text: return;
        case element_type::chain: {
            /// First, flatten each element.
            for (auto& item : els) item.flatten();

            /// If this chain contains only one element, reduce to that.
            if (els.size() == 1) {
                els[0].pos = pos;
                reduce_to_first_element();
                return;
            }

            /// Otherwise, multiply all containing elements that are text or class elements.
            /// First, determine the position of the first text or class element.
            u64 last_lhs = U64_MAX;
            for (u64 idx = 0; idx < els.size(); idx++) {
                if (els[idx].type == element_type::text || els[idx].type == element_type::cclass) {
                    last_lhs = idx;
                    break;
                }
            }
            if (last_lhs == U64_MAX || last_lhs == els.size() - 1) return;

            /// Then, merge all text elements after that.
            for (u64 idx = 1; idx < els.size(); idx++) {
                auto& el = els[idx];

                /// If this element is neither text nor class, skip it, and unset last_lhs.
                if (el.type != element_type::cclass && el.type != element_type::text) {
                    last_lhs = U64_MAX;
                    continue;
                }

                /// Otherwise, make sure this element is a class.
                if (el.type == element_type::text) el = wrap<element_type::cclass>(std::move(el));

                /// If we don't have a last_lhs, set it to this element and continue;
                if (last_lhs == U64_MAX) {
                    last_lhs = idx;
                    continue;
                }

                /// Otherwise, merge them. Don't forget to decrement the
                /// index since we're removing one element.
                els[last_lhs].cl_cartesian_product(std::move(el), nullptr);
                els.erase(els.begin() + static_cast<i64>(idx--));
            }

            if (els.size() == 1) {
                els[0].pos = pos;
                reduce_to_first_element();
            }
            return;
        }
        case element_type::list:
        case element_type::cclass:
            /// First, flatten each element.
            for (auto& item : els) item.flatten();

            /// Merge classes into classes so long as they don't contain percentages.
            if (type != element_type::list && els.size() > 1) {
                for (u64 i = 0; i < els.size(); i++) {
                    if (els[i].type != element_type::cclass || els[i].percentage != 0.0f) continue;
                    auto sz = els[i].els.size();
                    els.insert(els.begin() + ssize_t(i), els[i].els.begin(), els[i].els.end());
                    i += sz;
                    els.erase(els.begin() + ssize_t(i));
                }
            }

            /// Only collapse classes that contain only one element.
            if (els.size() != 1) return;

            /// If this is a list and containing only one non-percentage-based
            /// class, then its elements become our elements. Otherwise, don't
            /// do anything.
            if (type == element_type::list) {
                if (els[0].type == element_type::cclass && els[0].percentage == 0) {
                    auto new_els = std::move(els[0].els);
                    els          = std::move(new_els);
                }
                return;
            }

            /// If we're not percentage-based, reduce.
            if (percentage == 0) return reduce_to_first_element();

            /// If our percentage is 100, then we keep that of our first element
            /// if that element is percentage-based, or 100 if it is not.
            if (percentage == 100) {
                if (els[0].percentage == 0) els[0].percentage = 100;
                return reduce_to_first_element();
            }

            /// Keep our percentage if our first element's percentage
            /// is 0 or 100.
            if (els[0].percentage == 0 || els[0].percentage == 100) {
                els[0].percentage = percentage;
                return reduce_to_first_element();
            }

            /// Otherwise, do not reduce.
            return;
        case element_type::negative:
            els[0].flatten();
            return;
        case element_type::optional:
            /// Optionals can be nested.
            els[0].flatten();
            if (els[0].type == element_type::optional) reduce_to_first_element();
            return;
    }
    ASSERT(false, "Invalid element type '{}'", u32(type));
}

void parser::element::reverse() {
    switch (type) {
        case element_type::empty:
        case element_type::wildcard:
        case element_type::word_boundary:
        case element_type::syllable_boundary: return;
        case element_type::text: text = text.reverse(); return;
        case element_type::list:
        case element_type::chain:
        case element_type::cclass:
            for (auto& el : els) el.reverse();
            std::reverse(els.begin(), els.end());
            return;
        case element_type::negative:
        case element_type::optional:
            ASSERT(!els.empty());
            els[0].reverse();
            return;
    }
    ASSERT(false, "Invalid element type '{}'", u32(type));
}

bool parser::element::postprocess_percentages() {
    if (type == element_type::cclass) {
        /// Determine the number of remaining percentage points and divide them among
        /// the elements without explicit percentages.
        float remaining_perc_points = 100.f;
        u32   items_without_percentage{};
        for (auto& item : els) {
            if (item.percentage == 0) items_without_percentage++;
            remaining_perc_points -= item.percentage;
        }
        remaining_perc_points /= float(items_without_percentage);
        for (auto& item : els)
            if (item.percentage == 0)
                item.percentage = remaining_perc_points;

        /// Sort by percentages in ascending order.
        std::sort(els.begin(), els.end(), [](cref a, cref b) { return a.percentage < b.percentage; });

        /// Lastly, add up the percentages in such a way that the first element has
        /// the lowest percentage, and the last element 100%.
        if (els.size() > 1)
            for (u64 i = 0; i < els.size() - 1; i++)
                els[i + 1].percentage += els[i].percentage;
    }

    /// Now do the same thing for all subelements.
    return std::all_of(els.begin(), els.end(), [](ref e) { return e.postprocess_percentages(); });
}

/// ====================================================================== ///
///  `parser::rule` Implementation                                         ///
/// ====================================================================== ///

auto parser::rule::str(u64 depth) const -> std::u32string {
    /// Input and output if applicable.
    std::u32string str = indent(depth);
    if (type != rule_type::epenthesis) str += fmt::format(U"(input\n{})", input.str(depth + 1));
    if (type == rule_type::substitution) str += fmt::format(U"\n{}(output\n{})", indent(depth), output.str(depth + 1));

    /// Element before the underscore.
    auto ctx_pre_str = ctx.pre.str(depth + 1);
    if (!ctx_pre_str.empty()) str += fmt::format(U"\n{}(context-pre\n{})", indent(depth), ctx_pre_str);

    /// Element after the underscore.
    auto ctx_post_str = ctx.post.str(depth + 1);
    if (!ctx_post_str.empty()) str += fmt::format(U"\n{}(context-post\n{})", indent(depth), ctx_post_str);

    return str;
}

///
/// Validation and postprocessing
///

/// Validate rule invariants.
/// ASSERT is used to check for parser bugs.
/// seterr is used for validating user input.
bool parser::rule::validate(parser& p) const {
    switch (type) {
        /// Reduplication, deletion, and metathesis rules must have an input.
        case rule_type::reduplication:
        case rule_type::deletion:
        case rule_type::metathesis:
            ASSERT(!input.empty(), "Input of reduplication, deletion, or metathesis must not be empty");
            ASSERT(input.type == element_type::list, "Input of reduplication, deletion, or metathesis rule must be a list");
            return true;

        /// The input and output of substitution rules must have the same number of elements,
        /// unless the output has exactly one element.
        case rule_type::substitution:
            ASSERT(!input.empty(), "Input of substitution rule must not be empty");
            ASSERT(!output.empty(), "Output of substitution rule must not be empty");
            ASSERT(input.type == element_type::list, "Input of substitution rule must be a list");
            ASSERT(output.type == element_type::list, "Output of substitution rule must be a list");
            if (output.size() > 1 && input.size() != output.size()) {
                p.seterr(output.pos, "Input and output must have the same number of elements when the output contains more than one element");
                p.note(input.pos, "Input has {} element{}; output has {} element{}",
                    input.size(), (input.size() == 1 ? "" : "s"),
                    output.size(), (output.size() == 1 ? "" : "s"));
                return false;
            }
            return rule::validate_percentages(p);

        /// Epenthesis rules must have no input and an output of size 1.
        /// Also issue a warning if the context is empty since that's probly not intended.
        case rule_type::epenthesis:
            ASSERT(input.empty(), "Input of epenthesis rule must be empty");
            if (output.size() != 1) {
                p.seterr(output.pos, "Output of epenthesis rule must consists of a single element");
                return false;
            }
            if (ctx.pre.empty() && ctx.post.empty())
                p.warning(warn_epenthesis_context, ctx.pos, "Context of epenthesis rule is empty");
            return rule::validate_percentages(p);
        default: ASSERT(false, "Invalid rule type");
    }
}

bool parser::rule::validate_percentages(parser& p) const {
    std::function<bool(const element&)> validate_class = [&p, &validate_class](const element& el) {
        float perc{};
        u64   items_without_percentage{};
        for (const auto& item : el.els) {
            if (item.type == element_type::cclass && !validate_class(item)) return false;
            perc += item.percentage;
            if (item.percentage == 0) items_without_percentage++;
        }
        if (perc > 100) return p.seterr(el.pos, "Sum of percentages must not be greater than 100%; was {}%", perc);
        if (perc == 100 && items_without_percentage)
            return p.seterr(el.pos, "Excess elements are not permitted if all the elements with percentages already add up to 100%");
        return true;
    };

    return validate_class(output);
}

bool parser::rule::postprocess(parser& p) {
    /// If the output contains only one element, copy it for each input element.
    if (type == rule_type::substitution && output.size() == 1)
        for (u64 i = 0; i < input.size() - 1; i++)
            output.els.push_back(output.els.front());

    /// Only substitution and epenthesis rules may contain percentages.
    if (type != rule_type::substitution && type != rule_type::epenthesis) return true;
    return std::all_of(output.els.begin(), output.els.end(), [](element& e) { return e.postprocess_percentages(); });
}

/// ====================================================================== ///
///  `parser` Implementation -- Miscellaneous Parse Functions              ///
/// ====================================================================== ///

bool parser::add_rule(rule&& r) {
    /// Flatten everything.
    r.input.flatten();
    r.output.flatten();
    r.ctx.pre.flatten();
    r.ctx.post.flatten();

    /// Then, validate the rule and postprocess.
    if (!r.validate(*this)) return false;
    if (!r.postprocess(*this)) return false;

    rules.push_back(r);
    return true;
}

auto parser::class_name_to_class() -> element {
    ASSERT(tok.type == token_type::class_name);
    element c = classes.at(tok.text.data);
    c.pos     = tok.pos;
    discard();
    return c;
}

bool parser::parse_rules(const std::u32string& input) {
    init(input);
    tempset parsing_rule = true;

    while (tok.type != token_type::end_of_string) {
        while (tok.type == token_type::newline) discard();
        if (tok.type == token_type::end_of_string) break;
        if (!parse_rule()) return false;
    }
    return true;
}

bool parser::parse_classes(const std::u32string& input) {
    init(input);
    tempset parsing_rule = false;

    if (tok.type == token_type::newline) discard();
    while (tok.type != token_type::end_of_string)
        if (!parse_class_def()) return false;
    return true;
}

auto parser::parse_words(const std::u32string& str) -> parse_result<std::vector<word>> {
    static const std::u32string space = U" ";
    std::vector<word>           ws;
    init(str);

    /// Set this to true so that we get END OF LINE tokens.
    tempset parsing_rule = true;
    for (;;) {
        word curr;
        while (tok.type == parser::token_type::text) {
            if (!curr.data.empty()) curr += space;
            curr += tok.text;
            next_token();
        }
        if (!curr.data.empty()) ws.push_back(std::move(curr));
        while (tok.type == parser::token_type::newline) next_token();
        if (tok.type == parser::token_type::end_of_string) break;
        else if (tok.type == parser::token_type::text) continue;
        else {
            error(tok.pos, "Character(s) not allowed in word: \"{}\"", tok.str());
            return false;
        }
    }
    return ws;
}

/// ====================================================================== ///
///  `parser` Implementation -- SCA++ Grammar Parser                       ///
/// ====================================================================== ///

/// <rule> ::= <substitution-rule>
///          | <epenthesis-rule>
///          | <deletion-rule>
///          | <metathesis-rule>
///          | <reduplication-rule>
auto parser::parse_rule() -> bool {
    /// Epenthesis rules are the only rules that do not have an <input>.
    if (tok.type == token_type::separator) return parse_epenthesis_rule();

    /// If it's not an epenthesis rule, it has to have an <input>.
    auto [input, success] = parse_input();
    if (!success) return false;

    /// Next, all rules consume a SEPARATOR.
    if (!consume(token_type::separator)) return false;

    /// The rule type can now be determined by the token we're looking at.
    switch (tok.type) {
        case token_type::separator: return parse_deletion_rule(std::move(input));
        case token_type::ampersand: return parse_metathesis_rule(std::move(input));
        case token_type::plus: return parse_reduplication_rule(std::move(input));
        default: return parse_substitution_rule(std::move(input));
    }
}

/// <class-def>  ::= TEXT [ "=" ] <simple-el>
auto parser::parse_class_def() -> bool {
    /// Class redefinitions are not allowed.
    if (tok.type == token_type::class_name) return seterr(tok.pos, "Redefinition of class \"{}\"", to_utf8(tok.text.data));

    /// Make sure the class name is TEXT.
    if (tok.type != token_type::text) return seterr(tok.pos, "Expected class name here, but got {}", tok.str());

    /// If a class with this name is already defined, but somehow, the token
    /// is of type text anyway, then that's a bug in the lexer.
    ASSERT(!classes.contains(tok.text.data), "text '{}' should have been a class_name\n    {}", to_utf8(tok.text.data),
        diagnostic{this, tok.pos, diagnostic_level::note, "Here"}.message());

    /// Save this for later.
    std::u32string class_name = tok.text.data;
    auto           start      = tok.pos.start;
    if (!advance()) return seterr(tok.pos, "Incomplete class definition");

    /// Handle "=".
    if (tok.type == token_type::assign && !advance()) return seterr(tok.pos, "Missing element after = in class definition");

    /// Parse the class.
    auto [el, success] = parse_simple_element();
    if (!success || has_error) return false;

    /// Define it. Make sure to make it a class if it isn't already one.
    if (el.type != element_type::cclass) el = element::wrap<element_type::cclass>(std::move(el));
    el.flatten();
    el.pos.start        = start;
    classes[class_name] = std::move(el);
    return true;
}

/// <substitution-rule>  ::= <input> SEPARATOR . <output> <context>
auto parser::parse_substitution_rule(element&& input) -> bool {
    rule r{rule_type::substitution};

    /// Parse the output.
    if (!set_if_success(r.output, parse_output())) return false;

    /// Next, the context.
    if (!set_if_success(r.ctx, parse_context())) return false;

    /// Add the rule to the list of rules.
    r.input = std::move(input);
    return add_rule(std::move(r));
}

/// <epenthesis-rule>    ::= . SEPARATOR <output> <context>
auto parser::parse_epenthesis_rule() -> bool {
    rule r{rule_type::epenthesis};

    /// Yeet SEPARATOR.
    if (!consume(token_type::separator)) return seterr(tok.pos, "Expected SEPARATOR at start of epenthesis rule");

    /// Parse the output.
    if (!set_if_success(r.output, parse_output())) return false;

    /// Next, the context.
    if (!set_if_success(r.ctx, parse_context())) return false;

    /// Add the rule to the list of rules.
    return add_rule(std::move(r));
}

/// <deletion-rule>      ::= <input> SEPARATOR . <context>
auto parser::parse_deletion_rule(element&& input) -> bool {
    rule r{rule_type::deletion};

    /// Parse the context.
    if (!set_if_success(r.ctx, parse_context())) return false;

    /// Add the rule to the list of rules.
    r.input = std::move(input);
    return add_rule(std::move(r));
}

/// <metathesis-rule>    ::= <input> SEPARATOR . "&" <context>
auto parser::parse_metathesis_rule(element&& input) -> bool {
    rule r{rule_type::metathesis};

    /// Yeet "&".
    if (!consume(token_type::ampersand)) return false;

    /// Parse the context.
    if (!set_if_success(r.ctx, parse_context())) return false;

    /// Add the rule to the list of rules.
    r.input = std::move(input);
    return add_rule(std::move(r));
}

/// <reduplication-rule> ::= <input> SEPARATOR . "+" { "+" } <context>
auto parser::parse_reduplication_rule(element&& input) -> bool {
    rule r{rule_type::reduplication};

    /// Count the number of "+" signs.
    while (tok.type == token_type::plus) {
        r.reps++;
        if (!advance()) return false;
    }

    /// Next, the context.
    if (!set_if_success(r.ctx, parse_context())) return false;

    /// Add the rule to the list of rules.
    r.input = std::move(input);
    return add_rule(std::move(r));
}

/// <input>          ::= <input-els>  { "," <input-els> }
/// <output>         ::= <output-els> { "," <output-els> }
/// <simple-el-list> ::= <simple-els> { "," <simple-els> }
template <auto parse_fun, auto token_pred, constexpr_string token_pred_err_msg>
auto parser::parse_comma_separated() -> parse_result<element> {
    /// The input is always a list.
    element element;
    element.type = element_type::list;

    /// Parse the elements.
    for (;;) {
        if (!append_if_success(element.els, (this->*parse_fun)()))
            return false; /// parse_fun should have already reported the error.

        /// If the next token isn't a comma, return the list.
        if (tok.type != token_type::comma) {
            element.pos.start = element.els[0].pos.start;
            element.pos.end   = element.els.back().pos.end;
            return element;
        }

        /// Otherwise, keep parsing so long as there is a comma.
        /// Yeet ",".
        if (!advance()) return seterr(tok.pos, "Expected element after comma.");
        if (!(tok.*token_pred)()) return seterr(tok.pos, "{}", token_pred_err_msg.data);
    }
}

/// <input>      ::= <input-els>  { "," <input-els> }
auto parser::parse_input() -> parse_result<element> {
    return parse_comma_separated<&parser::parse_input_elements,
        &parser::token::can_be_start_of_input_el,
        "Element after comma must be a class or TEXT">();
}

/// <output>     ::= <output-els> { "," <output-els> }
auto parser::parse_output() -> parse_result<element> {
    return parse_comma_separated<&parser::parse_output_elements,
        &parser::token::can_be_start_of_output_el,
        "Element after comma must be an optionally percentage-decorated class or TEXT">();
}

/// <context>    ::= SEPARATOR [ <ctx-els> ] USCORE { USCORE } [ <ctx-els> ] <eol>
auto parser::parse_context() -> parse_result<rule::context> {
    rule::context ctx;

    /// Yeet the separator.
    /// We shouldn't include the separator before the context in its range.
    if (!consume(token_type::separator)) return seterr(tok.pos, "Expected SEPARATOR before context, got {}", tok);

    /// If the next token is not an USCORE, parse <ctx-els>.
    if (tok.type != token_type::uscore && !set_if_success(ctx.pre, parse_context_elements()))
        return seterr(tok.pos, "Expected UNDERSCORE or element in context");

    /// Now, we consume an USCORE.
    location uscore_start_pos = tok.pos.start;
    location uscore_end_pos;
    if (tok.type != token_type::uscore) return seterr(tok.pos, "Expected UNDERSCORE in context, got {}", tok);
    while (tok.type == token_type::uscore) {
        uscore_end_pos = tok.pos.end;
        discard();
    }

    /// If the next token is not a boundary character or <eol>, parse <ctx-els>.
    /// Then, parse the boundaries at the end of the context.
    if (!tok.eol() && !set_if_success(ctx.post, parse_context_elements()))
        return seterr(tok.pos, "Expected END OF LINE, or element, but got {}", tok);

    /// A rule must be terminated by an <eol>.
    if (!tok.eol()) return seterr(tok.pos, "A rule must be terminated by END OF LINE");

    /// Determine the start of the context's range.
    if (!ctx.pre.empty()) ctx.pos.start = ctx.pre.pos.start;
    else ctx.pos.start = uscore_start_pos;

    /// Determine the end of the context's range.
    if (!ctx.post.empty()) ctx.pos.end = ctx.post.pos.end;
    else ctx.pos.end = uscore_end_pos;

    return ctx;
}

/// <input-els>    ::= { <input-el> }-
/// <output-els>   ::= { <output-el> }-
/// <optional-els> ::= { <optional-el> }-
/// <simple-els>   ::= { <simple-el> }-
template <auto parse_fun, auto token_pred, constexpr_string parse_fun_err_msg, constexpr_string no_element_err_msg>
auto parser::parse_chain() -> parse_result<element> {
    element els;
    els.type = element_type::chain;

    /// We need at least one element.
    if (!(tok.*token_pred)()) return seterr(tok.pos, "{}", no_element_err_msg.data);
    do {
        auto l = loc;
        if (!append_if_success(els.els, (this->*parse_fun)())) return seterr({l, l}, "{}", parse_fun_err_msg.data);
    } while ((tok.*token_pred)());

    /// Set up the range properly.
    els.pos.start = els.els.data()->pos.start;
    els.pos.end   = els.els.back().pos.end;
    return els;
}

/// <input-els>  ::= { <input-el> }-
auto parser::parse_input_elements() -> parse_result<element> {
    return parse_chain<&parser::parse_input_element,
        &parser::token::can_be_start_of_input_el,
        "At least one input element is required",
        "Input element must be \"*\", TEXT or class">();
}

/// <input-el>   ::= <simple-el> | "*"
auto parser::parse_input_element() -> parse_result<element> {
    /// If the current token is "*", then we're done.
    if (tok.type == token_type::wildcard) {
        discard(); /// Yeet "*".
        element el;
        el.type = element_type::wildcard;
        return el;
    }

    /// Otherwise, parse a <simple-el>.
    return parse_simple_element();
}

/// <output-els> ::= { <output-el> }-
auto parser::parse_output_elements() -> parse_result<element> {
    auto [el, success] = parse_chain<&parser::parse_output_element,
        &parser::token::can_be_start_of_output_el,
        "At least one output element is required",
        "Output element must be TEXT or optionally percentage-decorated class">();
    if (success) return std::move(el);

    /// If we fail to parse an output element, it might be because of a
    /// missing separator before the context.
    if (tok.boundary_char()
        || tok.type == token_type::uscore
        || tok.type == token_type::lparen
        || tok.type == token_type::tilde
        || tok.eol()) note(tok.pos, "Did you forget to put a \"/\" here?");
    return false;
}

/// <output-el>  ::= <percent-alternatives> | <simple-el>
auto parser::parse_output_element() -> parse_result<element> {
    /// Output elements may contain percentages.
    if (tok.type == token_type::percentage) return parse_percent_alternatives();

    /// Otherwise, just parse a <simple-el>.
    return parse_simple_element();
}

/// <boundaries> ::= { "#" | "$" }
auto parser::parse_boundaries() -> element {
    range r = tok.pos;
    if (tok.type == token_type::hash) {
        discard();
        return element{element_type::word_boundary, r};
    } else if (tok.type == token_type::dollar) {
        discard();
        return element{element_type::syllable_boundary, r};
    } else return {};
}

///< ctx-els> ::= <decorated-els>
auto parser::parse_context_elements() -> parse_result<element> {
    /// This is just a <decorated-els>.
    return parse_decorated_elements();
}

/// <decorated-els> ::= { <decorated-el> }+
auto parser::parse_decorated_elements() -> parse_result<element> {
    return parse_chain<&parser::parse_decorated_element,
        &parser::token::can_be_start_of_decorated_el,
        "At least one element is required here",
        "TEXT, class, \"(\", or \"~[\" expected here">();
}

/// <decorated-el> ::= <simple-el>
///                  | <boundaries>
///                  |     "("  <decorated-els> ")"
///                  | "~" "["  <decorated-els> "]"
auto parser::parse_decorated_element() -> parse_result<element> {
    /// If we have "(", then parse an <optional-els>.
    if (tok.type == token_type::lparen) {
        auto start = tok.pos.start;
        if (!advance()) return seterr(tok.pos, "Expected at least one element after \")\""); /// Yeet "(".

        /// Parse a <decorated-els>.
        auto l             = loc;
        auto [el, success] = parse_decorated_elements();
        if (!success) return seterr({l, l}, "Could not parse elements after \"(\"");

        /// Make sure we have a ")" here.
        /// Then, wrap the element and return it.
        auto end_pos = tok.pos.end;
        if (!consume(token_type::rparen)) {
            if (!has_error) {
                seterr(tok.pos, "Expected \")\" here");
                note({start, start}, "To match this \"(\"");
            }
            return false;
        }
        auto wrapped = element::wrap<element_type::optional>(std::move(el));
        wrapped.pos  = {start, end_pos};
        return wrapped;
    }

    /// If we have a tilde, then this is a negated element.
    if (tok.type == token_type::tilde) {
        /// Make sure we have a "[" after the tilde.
        auto start = tok.pos.start;
        if (!advance() || tok.type != token_type::lbrack)
            return seterr(tok.pos, "Operator ~ must be preceded by an element or followed by \"[\"");

        /// Make sure we have at least one element after the "["
        if (!advance()) return seterr(tok.pos, "Expected at least one element after \"[\"");

        /// Parse a <decorated-els>.
        auto l             = loc;
        auto [el, success] = parse_decorated_elements();
        if (!success) return seterr({l, l}, "Could not parse elements after \"[\"");

        /// Make sure we have a "]" here.
        auto end_pos = tok.pos.end;
        if (!consume(token_type::rbrack)) {
            if (!has_error) {
                seterr(tok.pos, "Expected \"]\" here");
                note({start, start}, "To match this \"[\"");
            }
            return false;
        }

        /// Wrap the element and return it.
        auto negated = element::wrap<element_type::negative>(std::move(el));
        negated.pos  = {start, end_pos};
        return negated;
    }

    /// It might also be a word boundary.
    if (tok.boundary_char()) return parse_boundaries();

    /// Otherwise, this is just an <input-el>.
    return parse_input_element();
}

/// <percent-alternatives> ::= PERCENTAGE <percent-class>
auto parser::parse_percent_alternatives() -> parse_result<element> {
    /// It's a logical error to call this function if we don't know for sure that the parser is looking at a percentage.
    /// Ensuring that we never do that lets us parse this without lookahead.
    ASSERT(tok.type == token_type::percentage, "Illegal parser state. Expected percentage, got {}", tok.str());

    /// Save this for later
    auto perc_loc = tok.pos.start;
    auto perc     = tok.number;
    if (perc > 100) return seterr(tok.pos, "Percentages with a value greater than 100% are not allowed");
    if (!advance()) return seterr(tok.pos, "Percentage must be followed by an element");

    /// Parse a <percent-class>
    if (tok.type != token_type::lbrace) return seterr(tok.pos, "Alternatives after top-level percentage must be enclosed with \"{{}}\"");
    auto l = loc;
    if (auto [el, success] = parse_percent_class(); !success) return seterr({l, l}, "Percentage must be followed by an element");
    else {
        el.pos.start  = perc_loc;
        el.percentage = u8(perc);
        return std::move(el);
    }
}

/// <percent-class>        ::= "{" <percent-list> "}"
auto parser::parse_percent_class() -> parse_result<element> {
    /// It's a logical error to call this function if we don't know for sure that the parser is looking at a "{".
    /// Ensuring that we never do that lets us parse this without lookahead.
    ASSERT(tok.type == token_type::lbrace, "Illegal parser state. Expected \"{{\", got {}", tok.str());

    /// Yeet "{".
    auto start = tok.pos.start;
    if (!advance()) return seterr(tok.pos, "Unclosed \"{{\" at end of input");

    /// Then, parse a <percent-list>.
    /// The next token after that must be "}".
    if (auto [el, success] = parse_percent_list(); !success) return false;
    else {
        /// User might have put a percentage here.
        if (tok.type == token_type::percentage) return seterr(tok.pos, "Percentages are not allowed here");

        /// Yeet "}".
        auto end_pos = tok.pos.end;
        if (!consume(token_type::rbrace)) {
            if (!has_error) {
                seterr(tok.pos, "Missing \"}}\"");
                note({start, start}, "To match this \"{{\"");
            }
            return false;
        }

        /// Don't forget to set the type to class.
        el.type = element_type::cclass;
        el.pos  = {start, end_pos};
        return std::move(el);
    }
}

/// <percent-list>         ::= <percent-els> { "," <percent-els> }
auto parser::parse_percent_list() -> parse_result<element> {
    return parse_comma_separated<&parser::parse_percent_elements,
        &parser::token::can_be_start_of_percent_els,
        "Element after comma must be a PERCENTAGE, \"{\", or TEXT">();
}

/// <percent-els>          ::= [ PERCENTAGE ] { <percent-el> }-
auto parser::parse_percent_elements() -> parse_result<element> {
    const auto parse_percent_elements_chain = [this] {
        return parse_chain<&parser::parse_percent_element,
            &parser::token::can_be_start_of_percent_el,
            "TEXT or \"{\" required here",
            "Expected TEXT or \"{\"">();
    };

    if (tok.type == token_type::percentage) {
        /// Save this for later.
        auto perc_loc = tok.pos.start;
        auto perc     = tok.number;
        if (perc > 100) return seterr(tok.pos, "Percentages with a value greater than 100% are not allowed");
        if (!advance()) return seterr(tok.pos, "Percentage must be followed by an element");

        /// Parse <percent-els>.
        auto l = loc;
        if (auto [el, success] = parse_percent_elements_chain(); !success) return seterr({l, l},
            "Percentage must be followed by an element");
        else {
            el.pos.start  = perc_loc;
            el.percentage = u8(perc);
            return std::move(el);
        }
    }

    return parse_percent_elements_chain();
}

/// <percent-el>           ::= ( TEXT | <percent-class> )
auto parser::parse_percent_element() -> parse_result<element> {
    /// If this is just TEXT, return it.
    if (tok.type == token_type::text) return element::from_curr_text_tok(*this);

    /// Otherwise, this is a <percent-class>.
    return parse_percent_class();
}

/// <simple-el>            ::= TEXT | <simple-el-class>
auto parser::parse_simple_element() -> parse_result<element> {
    /// Percentages are not allowed here. Suggest a fix to the
    /// user depending on whether we're in a class or not.
    if (tok.type == token_type::percentage) {
        if (class_lbrace.line) return seterr_with_note(tok.pos, "Percentages are not allowed here",
            {class_lbrace, class_lbrace}, "Use \"%{\" here to allow percentages");
        return seterr(tok.pos, "Percentages can only occur within \"%{{}}\"");
    }

    /// If this is just TEXT, return it.
    if (tok.type == token_type::text) {
        auto pos = tok.pos;
        auto el  = element::from_curr_text_tok(*this);

        /// The user might have put a "~" here in the assumption
        /// that the lhs was actually a class.
        if (tok.type == token_type::tilde) {
            if (!has_error) {
                seterr(tok.pos, "Left-hand side of Operator ~ must be a class.");
                note(pos, "Are you sure this is a class?");
            }
            return false;
        }

        return el;
    }

    /// Otherwise, this is a <simple-el-class>.
    auto l = tok.pos;
    if (auto [el, success] = parse_simple_element_class(); !success) return seterr(l, "Expected TEXT, CLASS NAME, or \"{{\"");
    else return std::move(el);
}

/// <simple-el-class>      ::= <simple-el-class-lit> { <set-op> <simple-el-rhs> }
auto parser::parse_simple_element_class() -> parse_result<element> {
    /// First, parse a <simple-el-class-lit>.
    auto here                           = tok.pos;
    auto [class_lit, class_lit_success] = parse_simple_element_class_literal();
    if (!class_lit_success) return seterr(here, "Expected class name or \"{{\" here");

    /// So long as the next token is an operator, we try to evaluate the operation.
    while (tok.set_operator()) {
        /// We only allow one "+" here.
        if (tok.type == token_type::plus && tok.number != 1) return seterr(tok.pos, "Multiple \"+\" are not allowed here");

        /// Make sure we have a rhs.
        auto op_type = tok.type;
        auto op_pos  = tok.pos;
        if (!advance()) return seterr(tok.pos, "{} must be followed by a class or TEXT", set_operator_to_str(op_type)); /// Yeet operator.

        /// Parse the right-hand side.
        auto l             = loc;
        auto [el, success] = parse_simple_element_rhs();
        if (!success) return seterr({l, l}, "Operator \"{}\" must be followed by a class or TEXT", set_operator_to_str(op_type));

        /// Then, apply the operation.
        if (!class_lit.apply_class_operation(*this, std::move(el), op_type)) return seterr(op_pos, "Could not apply operation. This is probably a bug.");
    }

    return std::move(class_lit);
}

/// <simple-el-rhs> ::= <simple-el-class-lit> | TEXT
auto parser::parse_simple_element_rhs() -> parse_result<element> {
    /// If this is just TEXT, return it.
    if (tok.type == token_type::text) return element::from_curr_text_tok(*this);

    /// Otherwise, parse a <simple-el-class-lit>.
    return parse_simple_element_class_literal();
}

/// <simple-el-class-lit>  ::= CLASS-NAME | "{" <simple-el-list> "}"
auto parser::parse_simple_element_class_literal() -> parse_result<element> {
    /// If this is a CLASS-NAME, return it.
    if (tok.type == token_type::class_name) return class_name_to_class();

    /// Make sure we know that we're in a class.
    tempset class_lbrace = tok.pos.start;

    /// Otherwise, the next token must be "{".
    auto start = tok.pos.start;
    if (!consume(token_type::lbrace)) return false;

    /// Then, parse a <simple-el-list>.
    /// The next token after that must be "}".
    if (auto [el, success] = parse_simple_element_list(); !success) return false;
    else {
        auto end_pos = tok.pos.end;
        if (!consume(token_type::rbrace)) {
            if (!has_error) {
                seterr(tok.pos, "Missing \"}}\"");
                note({start, start}, "To match this \"{{\"");
            }
            return false;
        }
        el.pos = {start, end_pos};
        return std::move(el);
    }
}

/// <simple-el-list>       ::= <simple-els> { "," <simple-els> }
auto parser::parse_simple_element_list() -> parse_result<element> {
    auto l             = loc;
    auto [el, success] = parse_comma_separated<&parser::parse_simple_elements,
        &parser::token::can_be_start_of_simple_el,
        "Element after comma must be a class or TEXT">();
    if (!success) return seterr({l, l}, "Could not parse elements here");
    el.type = element_type::cclass;
    return std::move(el);
}

/// <simple-els>           ::= { <simple-el> }-
auto parser::parse_simple_elements() -> parse_result<element> {
    /// Percentages are not allowed here. Suggest a fix to the
    /// user depending on whether we're in a class or not.
    if (tok.type == token_type::percentage) {
        if (class_lbrace.line) return seterr_with_note(tok.pos, "Percentages are not allowed here",
            {class_lbrace, class_lbrace}, "Use \"%{\" here to allow percentages");
        return seterr(tok.pos, "Percentages can only occur within \"%{{}}\"");
    }

    return parse_chain<&parser::parse_simple_element,
        &parser::token::can_be_start_of_simple_el,
        "TEXT or class required here",
        "Expected TEXT or class">();
}

/// ====================================================================== ///
///  `parser` Implementation -- SCA2 Preprocessor and Driver               ///
/// ====================================================================== ///

///
/// Preprocessor
///

/// Parse SCA2 rewrite rules.
///
/// <rewrite-rule> ::= ANY+ "|" ANY+ EOL
///
/// Since the syntax of rewrite rules is so simple,
/// we don't need a sophisticated parser for this.
/// A simple state machine is more than enough.
bool parser::zompist_process_rewrite_rules() {
    enum {
        expecting_from,
        in_from,
        expecting_to,
        in_to,
    } state = expecting_from;
    std::u32string from;

    /// The start position of the line we're parsing;
    const char32_t* line_start_pos{};

    /// The start position of the element we're parsing.
    const char32_t* output_start_pos{};

    /// The location of the last "|". Used for warning messages.
    location vbar_loc{};

    /// The positions of the rewrite rules.
    struct rw_pos {
        const char32_t* start;
        u64             len;
    };
    std::vector<rw_pos> poss;

    /// Append and verify a rewrite rule.
    auto append_rw_rule = [&] {
        poss.push_back({line_start_pos, u64(where - line_start_pos) - 1});
        rewrite_rules.push_back({from, std::u32string{output_start_pos, u64(where - output_start_pos) - 1}});
        if (rewrite_rules.back().second.find(U'|') != std::u32string::npos)
            warning(warn_z_vbar_in_rw_rule, {vbar_loc, vbar_loc}, "\"|\" in the output of a rewrite rule might not be a good idea. "
                                                                  "Rewrite rules cannot be used to generate more rewrite rules.");
    };

    /// Parse the rewrite rules, skipping over anything that does not
    /// appear to be a rewrite rule.
    while (!at_end) {
        switch (state) {
            /// The input must not be empty, and newlines are ignored.
            case expecting_from:
                switch (lastc) {
                    case U'|': return seterr({loc, loc}, "Input of rewrite rule may not be empty");
                    case U'-': skip_line(); break;
                    case U'\n': break;
                    default:
                        line_start_pos = where - 1;
                        state          = in_from;
                }
                break;

            /// If "|" is encountered, save the input in `from`.
            /// If "\n" is encountered, then this line does not contain a rewrite rule.
            case in_from:
                switch (lastc) {
                    case U'|':
                        from  = std::u32string{line_start_pos, u64(where - line_start_pos) - 1};
                        state = expecting_to;
                        break;
                    case U'\n':
                        state = expecting_from;
                        break;
                }
                break;

            /// The output must not be empty.
            case expecting_to:
                switch (lastc) {
                    case U'\n': return seterr({loc, loc}, "Output of rewrite rule may not be empty");
                    case U'|':
                        vbar_loc = loc;
                        [[fallthrough]];
                    default:
                        output_start_pos = where - 1;
                        state            = in_to;
                }
                break;

            /// "\n" marks the end of a rewrite rule.
            case in_to:
                switch (lastc) {
                    case U'\n':
                        append_rw_rule();
                        from.clear();
                        state = expecting_from;
                        break;
                    case U'|':
                        vbar_loc = loc;
                        [[fallthrough]];
                    default: break;
                }
                break;
        }
        next_char();
    }

    /// Handle end of input properly.
    switch (state) {
        case expecting_to: return seterr({loc, loc}, "Output of rewrite rule may not be empty");
        case in_to:
            append_rw_rule();
            break;
        default: break;
    }

    /// Remove the rewrite rules from the input by replacing them with empty lines.
    /// This ensures that the line positions reported by the lexer remain
    /// unchanged for the rest of the input.
    for (auto it = poss.rbegin(); it != poss.rend(); ++it) input_text.erase(u64(it->start - input_text.data()), it->len);
    return true;
}

void parser::zompist_rewrite(std::u32string& z_text) {
    for (const auto& [from, to] : rewrite_rules) replace_all(z_text, from, to);
}

void parser::zompist_unrewrite(std::u32string& z_text) {
    for (const auto& [to, from] : rewrite_rules) replace_all(z_text, from, to);
}

///
/// SCA2 mode Driver
///

bool parser::zompist_parse(const std::u32string& z_text) {
    /// Switch to SCA2 mode.
    tempset sca2_mode = true;

    /// We need newlines.
    tempset parsing_rule = true;

    /// The 'stray diacritic' warning doesn't make sense in this mode.
    tempset warnings = warning_type{warnings & ~warn_stray_diacritic};

    /// Due to rewrite rules essentially acting like a preprocessor, we need to
    /// apply them to the input text before anything else.
    init_lexer(z_text);
    if (!zompist_process_rewrite_rules()) return false;
    zompist_rewrite(input_text);
    rewind();

    /// Parse SCA2 statements until the end of the string is reached or an error occurs.
    while (tok.type != token_type::end_of_string && !has_error) {
        /// Yeet empty lines.
        if (tok.type == token_type::newline) {
            discard();
            continue;
        }

        /// Parse a top level statement and return if an error was encountered.
        if (!zompist_top_level_statement()) return false;
    }

    return true;
}

/// ====================================================================== ///
///  `parser` Implementation -- SCA2 Grammar Parser                        ///
/// ====================================================================== ///

/// <sca2-expr> ::= <rule>
///               | <rewrite-rule>
///               | <class-def>
///               | <comment>
bool parser::zompist_top_level_statement() {
    /// Discard comments.
    if (tok.type == token_type::z_comment) {
        auto comment_start = tok.pos.start;
        discard();
        if (tok.type == token_type::wildcard)
            warning(warn_not_implemented, {comment_start, tok.pos.end}, "Intermediate results are currently not supported. "
                                                                        "Line will be treated as a comment and ignored");
        while (tok.type != token_type::end_of_string && tok.type != token_type::newline) discard();
        return true;
    }

    /// If the next token is a separator, then this is an epenthesis rule.
    else if (tok.type == token_type::separator || tok.type == token_type::z_arrow_separator)
        return zompist_epenthesis_rule();

    /// The next element must be a character or category.
    else if (!tok.can_be_start_of_z_char_or_cat())
        return seterr(tok.pos, "Unexpected token {} at top level", tok);

    /// This line contains a rule or class-def
    else {
        /// We need to look ahead to determine what exactly this is.
        auto first = std::move(tok);
        tok        = token{};
        if (!advance()) return seterr(tok.pos, "Unexpected end of input");

        switch (tok.type) {
            /// Now it's too late for rewrite rules
            case token_type::vbar: return seterr(tok.pos, "\"|\" is not allowed here");

            /// Category definition.
            case token_type::assign: return zompist_class_def(std::move(first));

            /// Anything else we try to parse as a rule.
            default: return zompist_rule(std::move(first));
        }
    }
}

/// <class-def> ::= CAPITAL "=" <chars-raw> EOL
bool parser::zompist_class_def(token&& class_name_tok) {
    /// Verify that we should be calling this function instead of something else.
    ASSERT_IS_TYPE(class_name_tok, text);
    ASSERT_IS_TYPE(tok, assign);

    /// Make sure the class name is valid.
    if (class_name_tok.text.size() != 1) return seterr(class_name_tok.pos, "Category name must be a single letter");

    /// Save this for later
    std::u32string class_name = {class_name_tok.text.data[0]};

    /// Yeet "=" and parse the contents of the class
    if (!advance()) return seterr(tok.pos, "Category definition must contain at least one character");
    classes[class_name] = zompist_chars_raw(tok.text_pos, class_name_tok.pos.start);
    discard(); /// Yeet "\n"

    /// Make sure we have at least one character.
    auto& cl = classes.at(class_name);
    if (cl.els.empty()) return seterr(tok.pos, "Category definition must contain at least one character");

    /// Set up the element correctly.
    cl.type      = element_type::cclass;
    cl.pos.start = class_name_tok.pos.start;
    cl.pos.end   = class_name_tok.pos.end;
    return true;
}

/// <rule> ::= <subst-rule>
///          | <deletion-rule>
///          | <metathesis-rule>
///          | <reduplication-rule>
///          | <epenthesis-rule>
///
/// Epenthesis rules are handled in zompist_top_level_statement().
bool parser::zompist_rule(token&& first_token) {
    /// Parse the input.
    auto [el, success] = zompist_input(std::move(first_token));
    if (!success) return false;

    /// Next, we expect a separator. The first separator may also be an arrow.
    if (!consume(token_type::separator) && !consume(token_type::z_arrow_separator))
        return seterr(tok.pos, "Expected {} or {} here", token_type::separator, token_type::z_arrow_separator);

    /// The type of the next token determines what rule we're looking at.
    switch (tok.type) {
        case token_type::separator: return zompist_deletion_rule(std::move(el));
        case token_type::z_dbl_backslash: return zompist_metathesis_rule(std::move(el));
        default: return zompist_substitution_or_reduplication_rule(std::move(el));
    }
}

/// <epenthesis-rule> ::= <i-separator> <output> <context>
bool parser::zompist_epenthesis_rule() {
    rule r{rule_type::epenthesis};

    /// First, we expect a separator. The first separator may also be an arrow.
    if (!consume(token_type::separator) && !consume(token_type::z_arrow_separator))
        return seterr(tok.pos, "Expected {} or {} here", token_type::separator, token_type::z_arrow_separator);

    /// Specialised error if the input and output are empty.
    if (tok.type == token_type::separator) return seterr(tok.pos, "Input or output is required");

    /// Parse the output.
    if (!set_if_success(r.output, zompist_output())) return false;

    /// Finally, the context.
    if (!zompist_context(r)) return false;

    /// Add the rule.
    return add_rule(std::move(r));
}

/// <deletion-rule> ::= <input>  <i-separator> . <del-context>
bool parser::zompist_deletion_rule(element&& input) {
    rule r{rule_type::deletion};

    /// Parse the deletion context. If it contains ², this function will also
    /// need to change the type of this rule to a substitution rule and add output
    /// to it, hence why it needs to be able to access the rule.
    if (!zompist_context(r)) return false;

    /// Add the rule.
    r.input = std::move(input);
    return add_rule(std::move(r));
}

/// <metathesis-rule> ::= <input> <i-separator> . "\\" <context>
bool parser::zompist_metathesis_rule(element&& input) {
    rule r{rule_type::metathesis};

    /// Yeet "\\".
    if (!consume(token_type::z_dbl_backslash)) return false;

    /// Parse the context.
    if (!zompist_context(r)) return false;

    /// Add the rule to the list of rules.
    r.input = std::move(input);
    return add_rule(std::move(r));
}

/// <subst-rule>         ::= <input> <i-separator> . <output>   <context>
/// <reduplication-rule> ::= <input> <i-separator> . <r-output> <context>
bool parser::zompist_substitution_or_reduplication_rule(element&& input) {
    rule r{rule_type::substitution};

    /// Parse the output. This might turn this into
    /// a reduplication rule instead.
    if (!zompist_substitution_or_reduplication_output(r)) return false;

    /// Parse the context.
    if (!zompist_context(r)) return false;

    /// Add the rule to the list of rules.
    r.input = std::move(input);
    return add_rule(std::move(r));
}

/// <input>    ::= <char-or-cat-list>
auto parser::zompist_input(token&& input) -> parse_result<element> {
    element chain;
    chain.type      = element_type::chain;
    chain.pos.start = input.pos.start;

    /// Due to the unfortunate way in which we handle the lookahead token here, we
    /// need to first manually parse it as a <char-or-cat>.
    if (!input.can_be_start_of_z_char_or_cat()) return seterr(tok.pos, "Expected character, category, or \"[\" in rule input");
    switch (input.type) {
        case token_type::text: {
            ASSERT(input.text.size() == 1, "Size of text token must be 1 in SCA2 mode");
            chain.els.push_back(element::from_text(std::move(input.text)));
        } break;
        case token_type::class_name: {
            element c = classes.at(input.text.data);
            c.pos     = input.pos;
            chain.els.push_back(std::move(c));
        } break;
        case token_type::lbrack: {
            auto [el, success] = zompist_category_literal(false, tok.pos.start);
            if (!success) return false;
            chain.els.push_back(std::move(el));
        } break;
        default: UNREACHABLE();
    }

    /// After we're done with input, continue parsing elements if there are any left.
    if (tok.can_be_start_of_z_char_or_cat()) {
        auto [rest, success] = zompist_char_or_category_list();
        if (!success) return false;
        chain.els.insert(chain.els.end(), std::make_move_iterator(rest.els.begin()), std::make_move_iterator(rest.els.end()));
        chain.pos.end = rest.pos.end;
    } else chain.pos.end = input.pos.end;
    return element::wrap<element_type::list>(std::move(chain));
}

/// <output>   ::= <char-or-cat-list>
auto parser::zompist_output() -> parse_result<element> {
    /// Parse the output.
    auto [el, success] = zompist_char_or_category_list();
    if (!success) return false;

    /// This must be a list.
    el.type = element_type::chain;
    return element::wrap<element_type::list>(std::move(el));
}

/// <r-output> ::= <output> | <cat> "²"
bool parser::zompist_substitution_or_reduplication_output(rule& r) {
    /// If the next token is not the start of a category, then this must be regular output.
    if (tok.type != token_type::class_name && tok.type != token_type::lbrack)
        return set_if_success(r.output, zompist_output());

    /// Otherwise, parse a category.
    auto [cat, cat_success] = zompist_category();
    if (!cat_success) return false;

    /// If the next token is ², then this is a reduplication rule.
    if (consume(token_type::z_superscript_2)) {
        /// Verify that the input and the category we've just parsed are the same.
        if (r.input.els != cat.els)
            return seterr(cat.pos, "Invalid reduplication rule: input of rule and category before the ² must be the same.");

        /// If everything is ok, then this is now a reduplication rule.
        r.type = rule_type::reduplication;
        return true;
    }

    /// Otherwise this is a regular substitution rule.
    /// Continue parsing outputs elements if there are any left.
    cat.type      = element_type::list;
    cat.pos.start = cat.pos.start;
    if (tok.can_be_start_of_z_char_or_cat()) {
        /// Append the remaining elements to the list.
        auto [rest, success] = zompist_char_or_category_list();
        if (!success) return false;
        cat.els.insert(cat.els.end(),
            std::make_move_iterator(rest.els.begin()),
            std::make_move_iterator(rest.els.end()));
        cat.pos.end = rest.pos.end;
    } else cat.pos.end = cat.pos.end;
    r.output = std::move(cat);
    return true;
}

/// <char-or-cat-list> ::= { <char-or-cat> }+
auto parser::zompist_char_or_category_list() -> parse_result<element> {
    /// Parse a chain of characters or categories.
    return parse_chain<&parser::zompist_char_or_category,
        &token::can_be_start_of_z_char_or_cat,
        "Expected character or category here",
        "At least one characters or category is required here">();
}

/// <char-or-cat> ::= CHAR | <cat>
auto parser::zompist_char_or_category() -> parse_result<element> {
    return tok.type == token_type::text
               ? element::from_curr_text_tok(*this)
               : zompist_category();
}

/// <cat> ::= CAT-NAME | <cat-lit>
auto parser::zompist_category() -> parse_result<element> {
    return tok.type == token_type::class_name
               ? class_name_to_class()
               : zompist_category_literal();
}

/// <cat-lit> ::= "[" { CHAR } "]"
auto parser::zompist_category_literal(bool needs_lbrack, location lbrack_pos) -> parse_result<element> {
    /// Consume a "[" if we haven't already done so.
    if (needs_lbrack) {
        lbrack_pos = tok.pos.start;
        if (!consume(token_type::lbrack)) return seterr(tok.pos, "Expected \"[\" at beginning of class literal");
    }

    /// We need at least one text token.
    if (tok.type != token_type::text) return seterr(tok.pos, "Expected characters in category literal, but got {}", tok);

    /// Then, parse characters and append them to a class.
    element el;
    el.type      = element_type::cclass;
    el.pos.start = lbrack_pos;
    while (tok.type == token_type::text) el.els.push_back(element::from_curr_text_tok(*this));

    /// The next token after all the characters must be a "]".
    el.pos.end = tok.pos.end;
    if (!consume(token_type::rbrack)) {
        if (!has_error) {
            seterr(tok.pos, "Missing \"]\"");
            note({lbrack_pos, lbrack_pos}, "To match this \"[\"");
        }
        return false;
    }

    return el;
}

/// <del-context> ::= SEPARATOR <decorated-ctx-els> USCORE [ "²" ] <decorated-ctx-els> [ SEPARATOR <exception> ] EOL
/// <context>     ::= SEPARATOR <decorated-ctx-els> USCORE         <decorated-ctx-els> [ SEPARATOR <exception> ] EOL
bool parser::zompist_context(rule& r) {
    /// First, we expect a separator.
    if (!consume(token_type::separator)) return seterr(tok.pos, "Expected separator / here");

    /// Parse the element before the uscore.
    if (!set_if_success(r.ctx.pre, zompist_decorated_context_elements())) return false;

    /// After that, we expect an uscore.
    auto uscore_pos = tok.pos;
    if (!consume(token_type::uscore)) return seterr(tok.pos, "Missing underscore in rule context");

    /// If this is a deletion rule and the next token is ², then this
    /// becomes a reduplication rule instead.
    auto superscript_2_pos = tok.pos.start;
    if (consume(token_type::z_superscript_2)) {
        /// Make sure the output is empty.
        if (r.type == rule_type::deletion) {
            if (!has_error) {
                seterr(tok.pos, "Superscript \"²\" is not allowed here.");
                note(r.output.pos, "Output must be empty when using \"_²\"");
            }
            return false;
        }

        /// This is now a reduplication rule.
        r.type        = rule_type::reduplication;
        r.output      = {};
        r.reps        = 1;
        r.ctx.pos.end = superscript_2_pos;
    }

    /// Determine the start position of the context
    if (!r.ctx.pre.empty()) r.ctx.pos.start = r.ctx.pre.pos.start;
    else r.ctx.pos.start = uscore_pos.start;

    /// Parse the element after the uscore.
    if (!set_if_success(r.ctx.post, zompist_decorated_context_elements())) return false;

    /// Determine the end position of the context
    if (!r.ctx.post.empty()) r.ctx.pos.end = r.ctx.post.pos.end;
    else r.ctx.pos.end = uscore_pos.end;

    /// If the next token is a separator, we have an exception context.
    if (consume(token_type::separator) && !zompist_context_exception(r)) return false;

    /// Next, we expect EOL.
    if (tok.type != token_type::end_of_string && !consume(token_type::newline))
        return seterr(tok.pos, "Excess {} at end of rule", tok);

    return true;
}

/// <exception> ::= <ctx-els> USCORE <decorated-ctx-els>
bool parser::zompist_context_exception(rule& r) {
    /// Parse the element before the underscore.
    /// Optionals are not allowed here.
    auto [pre_elem, pre_elem_success] = zompist_context_elements();
    if (!pre_elem_success) return false;

    /// Next, we expect an uscore.
    auto uscore_pos = tok.pos.end;
    if (!consume(token_type::uscore)) return false;

    /// Parse the element after the underscore.
    auto [post_elem, post_elem_success] = zompist_decorated_context_elements();
    if (!post_elem_success) return false;

    /// SCA2's exceptions translate to our negative elements.
    ///
    /// To implement exceptions, we prepend negated exception context elements
    /// to their corresponding elements in the main context.
    ///
    /// The output position should probably not include the exception elements,
    /// otherwise, our error messages will be really messy.
    if (!pre_elem.empty()) {
        pre_elem = element::wrap<element_type::negative>(std::move(pre_elem));
        pre_elem = element::wrap<element_type::chain>(std::move(pre_elem));
        pre_elem.els.push_back(std::move(r.ctx.pre));
        r.ctx.pre     = std::move(pre_elem);
        r.ctx.pre.pos = r.ctx.pre.els[1].pos;
    }

    if (!post_elem.empty()) {
        post_elem = element::wrap<element_type::negative>(std::move(post_elem));
        post_elem = element::wrap<element_type::chain>(std::move(post_elem));
        post_elem.els.push_back(std::move(r.ctx.post));
        r.ctx.post     = std::move(post_elem);
        r.ctx.post.pos = r.ctx.post.els[1].pos;

        /// The context position, however, should include the exception.
        r.ctx.pos.end = r.ctx.post.els[0].pos.end;
    } else r.ctx.pos.end = uscore_pos;

    return true;
}

/// <decorated-ctx-els> ::= { <decorated-ctx-el> }
auto parser::zompist_decorated_context_elements() -> parse_result<element> {
    /// No elements is fine in this case.
    if (!tok.can_be_start_of_z_decorated_context_element()) return element{};

    /// Otherwise, parse a chain of elements.
    return parse_chain<&parser::zompist_decorated_context_element,
        &token::can_be_start_of_z_decorated_context_element,
        "Expected element here",
        "Unreachable error message: parser::zompist_decorated_context_elements()">();
}

/// <decorated-ctx-el>  ::= <ctx-el> | "(" <decorated-ctx-el> ")"
auto parser::zompist_decorated_context_element() -> parse_result<element> {
    /// If we don't have a "(" here, then just parse a <ctx-el>.
    if (tok.type != token_type::lparen) return zompist_context_element();
    auto lparen_pos = tok.pos;

    /// Parse an optional element.
    if (!advance()) return seterr(tok.pos, "Expected element after \"(\""); /// Yeet "(".
    auto [el, success] = zompist_decorated_context_element();

    /// Yeet ")".
    if (!consume(token_type::rparen)) {
        if (!has_error) {
            seterr(tok.pos, "Missing \")\"");
            note(lparen_pos, "To match this \"(\"");
        }
        return false;
    }

    return std::move(el);
}

/// <ctx-els> ::= { <ctx-el> }
auto parser::zompist_context_elements() -> parse_result<element> {
    /// No elements is fine in this case.
    if (!tok.can_be_start_of_z_context_element()) return element{};

    /// Otherwise, parse a chain of elements.
    return parse_chain<&parser::zompist_context_element,
        &token::can_be_start_of_z_context_element,
        "Expected element here",
        "Unreachable error message: parser::zompist_context_elements()">();
}

/// <ctx-el> ::= "#" | <char-or-ctx-cat>
auto parser::zompist_context_element() -> parse_result<element> {
    if (consume(token_type::hash)) return element{element_type::word_boundary};
    return zompist_char_or_context_category();
}

/// <char-or-ctx-cat> ::= CHAR | CAT-NAME | "…" | <ctx-cat-lit>
auto parser::zompist_char_or_context_category() -> parse_result<element> {
    /// If it's a character, class name, or "…". then we're done.
    if (tok.type == token_type::z_ellipsis) return seterr(tok.pos, "Sorry, unimplemented. \"...\" is currently not supported");
    if (tok.type == token_type::text) return element::from_curr_text_tok(*this);
    if (tok.type == token_type::class_name) return class_name_to_class();

    /// Otherwise, parse a <ctx-cat-lit>.
    return zompist_context_category_literal();
}

/// <ctx-cat-lit> ::= "[" { <ctx-cat-lit-member> }+ "]"
auto parser::zompist_context_category_literal() -> parse_result<element> {
    /// Yeet "[".
    if (!consume(token_type::lbrack)) return seterr(tok.pos, "Expected element here");

    /// Parse the contained elements.
    return parse_chain<&parser::zompist_context_category_literal_member,
        &token::can_be_start_of_z_ctx_cat_lit_mem,
        "Expected character, category, or \"#\" here",
        "At least one character, category or \"#\" is required here">();
}

/// <ctx-cat-lit-member> ::= CHAR | CAT-NAME | "#"
auto parser::zompist_context_category_literal_member() -> parse_result<element> {
    /// Parse a character, class name, or "#".
    if (consume(token_type::hash)) return element{element_type::word_boundary};
    if (tok.type == token_type::text) return element::from_curr_text_tok(*this);
    if (tok.type == token_type::class_name) return class_name_to_class();

    /// Anything else is not allowed here.
    return seterr(tok.pos, "Unexpected token {}. Expected character, category, or \"#\"", tok);
}

/// <chars-raw> ::= { ANY }
auto parser::zompist_chars_raw(const char32_t* start_pos, location char_loc) -> element {
    element list;
    list.type = element_type::list;

    /// Append each character as a separate text element.
    while (start_pos < end && *start_pos != '\n') {
        element el;
        el.type = element_type::text;
        el.pos  = {char_loc, char_loc};
        el.text = std::u32string{*start_pos};
        char_loc.col++;
        list.els.push_back(std::move(el));
        start_pos++;
    }

    /// Make sure the parser is brought back in sync.
    skip_line();

    return list;
}

} // namespace sca