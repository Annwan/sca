#ifndef SCA_COMPILER_H
#define SCA_COMPILER_H

#include "parser.hh"

namespace sca {

#define FSM_TRY_REJECT (0xffffffff)

struct match_result {
    struct pos {
        u64 start{};
        u64 len{};
        u32 which{};
    };

    std::vector<pos> positions;
};

struct fsm {
    using bytecode_t = std::vector<u8>;

    /// FSM opcodes.
    /// Opcodes that start with 'i_r_' are the same as the corresponding
    /// 'i_' opcodes, except that they operate backwards.
    enum opcode : u8 {
        ///
        /// one: Match exactly one utf-32 codepoint.
        ///
        /// This instruction is followed by the codepoint to be matched.
        ///
        i_one,
        i_r_one,

        ///
        /// any: Match any one utf-32 codepoint.
        ///
        i_any,
        i_r_any,

        ///
        /// opt: Optionally match one element; skip if the match fails.
        ///
        /// The opt instruction is followed by an address and a submachine. The
        /// address indicates where to skip to if the submachine rejects.
        ///
        i_opt,

        ///
        /// try: Attempt to match one of multiple possible options.
        ///
        /// A try instruction is followed by an address that points
        /// to where to jump to once a match succeeds, and by a number
        /// of options after that.
        ///
        /// Each option is its own submachine and is preceded by an address
        /// that indicates where the start of the next option is located.
        ///
        /// An address of FSM_TRY_REJECT indicates the last option.
        ///
        i_try,

        ///
        /// neg: Match if a submachine rejects.
        ///
        /// The neg instruction is followed by an address and a submachine. This
        /// instruction fails if the submachine accepts (!). If the submachine
        /// rejects, it is skipped by jumping to the address.
        ///
        i_neg,

        ///
        /// start: Mark the start of a capture.
        ///
        /// This instruction marks the start of the capture.
        ///
        /// It is followed by a u32 that indicates the index in the top-level
        /// form that makes up this rule that is begin captured.
        ///
        i_capture_start,

        ///
        /// end: Mark the end of a capture.
        ///
        /// This instruction marks the end of the capture.
        ///
        i_capture_end,

        ///
        /// bnd: Match a word boundary.
        ///
        /// This instruction matches either the beginning or
        /// the end of the word.
        ///
        /// There is no reason to distinguish between these two
        /// since nothing can precede the beginning or follow the
        /// end of a word anyway.
        ///
        i_boundary,

        ///
        /// acc: Finish successfully.
        ///
        /// This instruction indicates that this (sub)machine has matched
        /// the input successfully and that it should stop executing.
        ///
        i_accept,

        ///
        /// rej: Terminate unsuccessfully.
        ///
        /// This instruction indicates that this (sub)machine could not
        /// match the input and that it should stop executing.
        ///
        i_reject,

        ///
        /// The instructions [i_match_class ... i_match_capture_4] MUST remain in this exact relative order.
        ///

        ///
        /// mcls*: Match a group that contains only single codepoints.
        ///
        /// This instruction is followed by a u32 that indicates the number
        /// of elements in the group to match, followed by the elements of the group
        ///
        /// match_class_2/3/4 are implemented analogously, except that they match
        /// 2/3/4-byte strings, respectively.
        ///
        i_match_class,
        i_match_class_2,
        i_match_class_3,
        i_match_class_4,

        i_r_match_class,
        i_r_match_class_2,
        i_r_match_class_3,
        i_r_match_class_4,

        ///
        /// mcap*: Like mcls*, but also sets the capture index.
        ///
        i_match_capture,
        i_match_capture_2,
        i_match_capture_3,
        i_match_capture_4,
    };

    void       dump() const;
    auto       interpret(const word& w) const -> match_result;
    bytecode_t bytecode;
};

struct match_context {
    const fsm&  machine;
    const word& w;
    const u8*   ip;

    const char32_t* start;
    const char32_t* curr;
    const char32_t* end;
    const char32_t* cap_start     = nullptr;
    const char32_t* cap_end       = nullptr;
    u32             capture_index = 0;

    void reset();

    match_context(const fsm& machine, const word& w);

    /// Run the bytecode interpreter and try to match the word.
    auto do_match() RELEASE_NOEXCEPT -> bool;
};

struct compiler {
    enum struct match_direction : bool {
        forwards,
        backwards,
    };

    /// Typically, we shouldn't try and recover from these
    /// since a compiler::error is indicative of a bug in the parser.
    /// FIXME: Yeet this and return true/false instead
    struct error : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

    fsm machine;

    /// Interface
    compiler() = default;

    template<match_direction>
    void compile_el(const parser::element& el);

    template<match_direction>
    void compile_capture_group(const parser::element& el);

    void dump();

    static auto compile(const parser& p) -> std::vector<fsm>;

    ///
    /// Backend
    ///
    u64 alloc_addr();

    /// Don't ask me why, but mainwindow.cc complains if
    /// we have this template declaration in the header.
#ifndef COMPILING_MAINWINDOW_CC
    template <fsm::opcode, match_direction, typename... args_t, bool... conds_t>
    void emit(args_t&&... args);
#endif

    template<match_direction>
    void emit_try(const parser::element::elements& el, bool capture = false);

    /// Emit i_match_class*
    template <u64 elems_length, match_direction>
    void emit_i_match_class(const parser::element& el);

    /// Emit i_match_capture*
    template <u64 elems_length, match_direction>
    void emit_i_match_capture(const parser::element& el);

    template <typename V>
    void store_addr(u64 addr, V value);
};

} // namespace sca
#endif // SCA_COMPILER_H
