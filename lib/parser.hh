#ifndef SCA_PARSER_HH
#define SCA_PARSER_HH

#include "cxx-utils.hh"

#include <compare>
#include <functional>
#include <iostream>
#include <map>
#include <vector>

#define TOKEN_TYPES(F)   \
    F(end_of_string)     \
    F(text)              \
    F(class_name)        \
    F(separator)         \
    F(arrow)             \
    F(lbrace)            \
    F(rbrace)            \
    F(lbrack)            \
    F(rbrack)            \
    F(lparen)            \
    F(rparen)            \
    F(hash)              \
    F(dollar)            \
    F(ampersand)         \
    F(plus)              \
    F(uscore)            \
    F(assign)            \
    F(tilde)             \
    F(comma)             \
    F(wildcard)          \
    F(vbar)              \
    F(percentage)        \
    F(newline)           \
    F(z_arrow_separator) \
    F(z_comment)         \
    F(z_dbl_backslash)   \
    F(z_superscript_2)   \
    F(z_ellipsis)

#define CONSTEXPR_ASSERT(condition, message)                                                                   \
    do {                                                                                                       \
        if constexpr (!(condition))                                                                            \
            ([]<bool flag = false> { static_assert(flag, "Assertion '" #condition "' failed! " message); }()); \
    } while (0)

namespace sca {
struct word {
    struct char_pos {
        u64 pos{};
        u64 len{};

        auto operator<=>(const char_pos&) const = default;
    };
    std::u32string        data;
    std::vector<char_pos> chars;

    word() = default;

    template <typename tstring>
    word(const tstring& t) {
        if (t.size() < 1) return;
        data = t;
        chars.clear();
        chars.push_back({0, t.size()});
    }

    [[nodiscard]] u64 size() const { return data.size(); }

    bool operator==(const word&) const = default;

    [[nodiscard]] auto reverse() const -> std::u32string;

    template <typename tstring>
    void operator+=(const tstring& t) {
        if (t.size() < 1) return;
        chars.push_back(char_pos{data.size(), t.size()});
        if constexpr (std::is_same_v<std::remove_cvref_t<tstring>, word>) data += t.data;
        else data += t;
    }

    template <typename tstring>
    word operator+(const tstring& t) const {
        if (t.size() < 1) return *this;
        word w{*this};
        w += t;
        return w;
    }
};

#define DECLARE_TOKEN_TYPE(x) x,

struct parser {
    enum struct token_type {
        invalid = 0,
        TOKEN_TYPES(DECLARE_TOKEN_TYPE)
    };

    enum struct element_type : u32 {
        empty,
        wildcard,
        word_boundary,
        syllable_boundary,
        text,
        list,
        chain,
        cclass,
        negative,
        optional,
    };

    enum struct rule_type : u32 {
        substitution,
        epenthesis,
        deletion,
        metathesis,
        reduplication,
    };

    enum struct diagnostic_level {
        raw, /// No colour
        note,
        warning,
        error,
    };

    enum warning_type : u32 {
        warn_stray_diacritic     = 1,
        warn_epenthesis_context  = 1 << 1,
        warn_empty               = 1 << 2,
        warn_pointless_operation = 1 << 3,
        warn_not_implemented     = 1 << 4,

        /// Only used in SCA2 mode.
        warn_z_vbar_in_rw_rule = 1 << 5,

        /// Disable all warnings.
        warn_none = 0,

        /// Enable all warnings.
        warn_all = 0b111111,
    };

    struct location {
        u32 line{};
        u32 col{};

        constexpr location() {}
        constexpr location(u32 l, u32 c) : line(l), col(c) {}
    };

    struct range {
        location start{};
        location end{};

        constexpr range() {}
        constexpr range(location s, location e) : start(s), end(e) {}
    };

    struct lexer_pos {
        const char32_t* where;
        char32_t        lastc;
        bool            at_end;
        std::u32string  last_text;
        location        loc;
        u32             last_line_end_col;
    };

    struct token {
        token_type      type = token_type::invalid;
        const char32_t* text_pos; /// Use with caution. Not guaranteed to be a valid pointer.
        range           pos;
        word            text;
        u64             number{}; /// Used by: plus, percentage.

        [[nodiscard]] bool boundary_char() const;
        [[nodiscard]] bool can_be_start_of_input_el() const;
        [[nodiscard]] bool can_be_start_of_decorated_el() const;
        [[nodiscard]] bool can_be_start_of_output_el() const;
        [[nodiscard]] bool can_be_start_of_percent_el() const;
        [[nodiscard]] bool can_be_start_of_percent_els() const;
        [[nodiscard]] bool can_be_start_of_simple_el() const;
        [[nodiscard]] bool can_be_start_of_z_char_or_cat() const;
        [[nodiscard]] bool can_be_start_of_z_context_element() const;
        [[nodiscard]] bool can_be_start_of_z_decorated_context_element() const;
        [[nodiscard]] bool can_be_start_of_z_ctx_cat_lit_mem() const;
        [[nodiscard]] bool eol() const;
        [[nodiscard]] auto spelling() const -> std::u32string;
        [[nodiscard]] auto str() const -> std::string;
        [[nodiscard]] bool set_operator() const;

        [[nodiscard]] static auto str(token_type type) -> std::string;
    };

    template <typename T>
    struct parse_result {
        T    value;
        bool success;

        parse_result(bool b) : success(false) { ASSERT(!b, "Do NOT pass true to parse_result."); }
        parse_result(T&& t) : value(t), success(true) {}
    };

    struct diagnostic {
        parser*          p;
        mutable range    pos;
        diagnostic_level level;
        std::string      msg;

        /// Seek to the line containing pos and return it.
        /// Also underline pos in that line.
        [[nodiscard]] auto line() const -> std::string;

        /// Generate a properly formatted diagnostic message.
        [[nodiscard]] auto message() const -> std::string;
    };

    struct element {
        using elements = std::vector<element>;
        using ref      = element&;
        using cref     = const element&;

        element_type type = element_type::empty;
        word         text;
        elements     els;
        float        percentage{};
        range        pos{};

        element() = default;
        element(element_type t);
        element(element_type t, range r);

        /// Compare two elements. This is mostly geared towards
        /// use in the difference operation.
        [[nodiscard]] bool operator==(const element& other) const;

        /// Construct the text for this element.
        /// This simply returns `text`, unless the element contains percentages,
        /// in which case this gets a bit more complicated.
        ///
        /// Calling this on anything other than an output element is an error.
        [[nodiscard]] auto construct_text() const -> std::u32string;

        /// Whether this element is actually empty.
        /// This can also return true, even if `type` isn't `empty`.
        [[nodiscard]] bool empty() const;

        /// Flatten an element.
        void flatten();

        /// Postprocess the percentages of an element for use in the applier.
        /// This MUST NOT be called without first flattening and validating the element.
        [[nodiscard]] bool postprocess_percentages();

        /// Replace this element with it's first member.
        void reduce_to_first_element() {
            ASSERT(!els.empty(), "Attempting to reduce while empty");

            /// Do NOT inline this variable. Inlining it *will* cause a heap-use-after-free
            /// error and crash this entire machinery quite horribly.
            els[0].pos = pos;
            auto el    = std::move(els[0]);
            *this      = std::move(el);
        }

        /// Reverse an element.
        void reverse();

        /// The number of subelements in this element.
        /// This has different semantics if this is a class based on whether
        /// `percentage` is equal to 0 or not.
        [[nodiscard]] u64 size() const;

        /// Format this element as an s-expression.
        [[nodiscard]] auto str(u64 depth = 0) const -> std::u32string;

        ///
        /// Set operations
        ///

        /// Apply an operation to this and another element.
        [[nodiscard]] bool apply_class_operation(parser& p, element&& rhs, token_type op);

        /// Construct the cartesian product between this element and another element.
        void cl_cartesian_product(element&& rhs, parser* p);

        /// Merge two groups into one by concatenating their elements;
        /// e.g. merging {a, b} and {c, d} yields {ac, bd}.
        void cl_catenate(element&& rhs, parser* p);

        /// Construct the set difference between this element and another element.
        void cl_difference(element&& rhs, parser* p);

        /// Construct the set intersection between this element and another element.
        void cl_intersection(element&& rhs, parser* p);

        /// Construct the union between this element and another element.
        void cl_union(element&& rhs, parser* p);

        ///
        /// Static functions
        ///

        /// Wrap `el` in an element of type `t`.
        template <element_type t>
        [[nodiscard]] static auto wrap(element&& el) -> element {
            CONSTEXPR_ASSERT(t == element_type::negative
                                 || t == element_type::optional
                                 || t == element_type::list
                                 || t == element_type::chain
                                 || t == element_type::cclass,
                "Cannot wrap with this element type");
            element decorator;
            decorator.type = t;
            decorator.pos  = el.pos;
            decorator.els.push_back(std::move(el));
            return decorator;
        }

        /// Convert the current token into a text element and discard it
        [[nodiscard]] static auto from_curr_text_tok(parser& p) -> element;

        /// Wrap text in an element
        [[nodiscard]] static auto from_text(word&& txt, range r = {}) -> element;
    };

    struct rule {
        struct context {
            element pre;
            element post;
            range   pos;
        };

        rule_type type;
        element   input;
        element   output;
        context   ctx;
        u64       reps{};

        explicit rule(){};
        explicit rule(rule_type t) : type(t) {}

        /// Postprocess the rule.
        [[nodiscard]] bool postprocess(parser& p);

        [[nodiscard]] auto str(u64 depth = 0) const -> std::u32string;
        [[nodiscard]] bool validate(parser& p) const;
        [[nodiscard]] bool validate_percentages(parser& p) const;
    };

    ///
    /// Lexer Variables
    ///

    /// The text that we're parsing.
    std::u32string input_text;

    /// The lexer's current position in the input.
    const char32_t* where;

    /// The end of the input.
    const char32_t* end;

    /// The last character read.
    char32_t lastc;

    /// The last text read.
    std::u32string last_text;

    /// The col/line position in the input.
    location loc;

    /// The last column of the previous line.
    u32 last_line_end_col;

    /// The last token read.
    token tok;

    /// Whether we've reached the end of the input.
    bool at_end;

    /// Whether we're parsing a rule.
    bool parsing_rule = false;

    /// Whether we're parsing SCA2 code.
    bool sca2_mode = false;

    /// Whether this parser uses terminal colours in messages.
    bool use_term_colours;

    /// Whether the default diagnostic handlers should emit diagnostics.
    /// You may choose to ignore this variable when providing your own handlers.
    bool diagnostics = true;

    /// Whether an error has been reported.
    bool has_error = false;

    /// The position of the top level left brace of the class we're currently parsing.
    location class_lbrace = {0, 0};

    /// Bitmask that controls what warnings are enabled.
    warning_type warnings = warn_all;

    /// Whether newly created parsers should default to using terminal colours in messages.
    static bool default_use_term_colours;

    /// The rewrite rules that we need to apply to the input/output.
    /// This is only used in SCA2 mode.
    std::vector<std::pair<std::u32string, std::u32string>> rewrite_rules;

    /// The classes we know about.
    std::map<std::u32string, element> classes;

    /// The rules that we have parsed so far.
    std::vector<rule> rules;

    /// Handle a diagnostic.
    std::function<void(diagnostic&&)> diagnostic_handler = default_diagnostic_handler;

    ///
    /// Initialisation
    ///

    /// Create a parser, but do not initialise it.
    explicit parser() { use_term_colours = default_use_term_colours; }

    /// Create a new parser with str as input.
    explicit parser(const std::u32string& str) {
        init(str);
        use_term_colours = default_use_term_colours;
    }

    /// Initialise the parser with _input.
    void init(const std::u32string_view& _input, u32 line_pos = 1, u32 col_pos = 0);

    /// Initialise the lexer with _input but do not start parsing.
    void init_lexer(const std::u32string_view& _input, u32 line_pos = 1, u32 col_pos = 0);

    ///
    /// Miscellaneous parser and lexer functions
    ///

    /// Validate and optimise a rule and add it to the rules vector.
    [[nodiscard]] bool add_rule(rule&& r);

    /// Discard the current token and return true if the token after that
    /// is not EOF. Otherwise, return false.
    [[nodiscard]] bool advance();

    /// Copy the class element corresponding to the current class name token.
    /// Also consume the token.
    [[nodiscard]] auto class_name_to_class() -> element;

    /// If the current token is of type `t`, discard it and return true.
    /// Otherwise, return false.
    [[nodiscard]] bool consume(token_type t);

    /// Consume the a token of type `t` and ensure that there are still more tokens left.
    [[nodiscard]] bool consume_and_advance(token_type t);

    /// Discard the current token and read the next one.
    void discard();

    /// Read the next character.
    void next_char();

    /// Read the next number.
    void next_number(u64 num, const char32_t* start_pos);

    /// Read the next text item.
    void next_text();

    /// Read the next token.
    void next_token();

    /// Restore the lexer position.
    void restore(const lexer_pos& p);

    /// Rewind to the beginning of the string.
    /// This also works if the input string has been modified.
    void rewind();

    /// Rewind only the lexer to the beginning of the string.
    void rewind_lexer();

    /// Save the lexer position.
    auto save() -> lexer_pos;

    /// Like seterr(...), but also print a note
    bool seterr_with_note(range _err_loc, std::string err_msg, range note_loc, std::string&& note_msg);

    /// Skip to the end of the line.
    void skip_line();

    /// Tokenise the input.
    auto tokens() -> std::vector<token>;

    /// Convert a class entry into a string representation.
    [[nodiscard]] static auto class_to_str(const std::u32string& name, const element& el) -> std::u32string;

    /// Default warning handler.
    static void default_diagnostic_handler(diagnostic&& d);

    /// Tokenise input.
    [[nodiscard]] static auto tokens(const std::u32string& input) -> std::vector<token>;

    /// Set the error message to msg if it's empty.
    /// Returns false for convenience.
    template <typename... args_t>
    bool seterr(range _err_loc, const fmt::format_string<args_t...>& f, args_t&&... args) {
        if (!has_error) {
            error(_err_loc, f, std::forward<args_t>(args)...);
            has_error = true;
        }
        return false;
    }

    /// Issue an error.
    template <typename... args_t>
    void error(range r, const fmt::format_string<args_t...>& f, args_t&&... args) {
        diagnostic_handler({this, r, diagnostic_level::error, fmt::format(f, std::forward<args_t>(args)...)});
    }

    /// Issue a message.
    template <typename... args_t>
    void note(range r, const fmt::format_string<args_t...>& f, args_t&&... args) {
        diagnostic_handler({this, r, diagnostic_level::note, fmt::format(f, std::forward<args_t>(args)...)});
    }

    /// Issue a message if w_type is enabled.
    template <typename... args_t>
    void note(warning_type w_type, range r, const fmt::format_string<args_t...>& f, args_t&&... args) {
        if (warnings & w_type) diagnostic_handler({this, r, diagnostic_level::note, fmt::format(f, std::forward<args_t>(args)...)});
    }

    /// Issue a warning.
    template <typename... args_t>
    void warning(warning_type w_type, range r, const fmt::format_string<args_t...>& f, args_t&&... args) {
        if (warnings & w_type) diagnostic_handler({this, r, diagnostic_level::warning, fmt::format(f, std::forward<args_t>(args)...)});
    }

    ///
    /// SCA++ Grammar Parser
    ///

    /// Parse templates.
    /// These are used to generate some of the parse functions below.
    template <auto parse_fun, auto token_pred, constexpr_string token_pred_err_msg>
    auto parse_comma_separated() -> parse_result<element>;

    template <auto parse_fun, auto token_pred, constexpr_string parse_fun_err_msg, constexpr_string no_element_err_msg>
    auto parse_chain() -> parse_result<element>;

    /// Driver
    [[nodiscard]] bool parse_rules(const std::u32string& input);
    [[nodiscard]] bool parse_classes(const std::u32string& input);
    [[nodiscard]] auto parse_words(const std::u32string& str) -> parse_result<std::vector<word>>;

    /// These all correspond to rules in the EBNF grammar.
    [[nodiscard]] bool parse_rule();
    [[nodiscard]] bool parse_class_def();

    [[nodiscard]] bool parse_epenthesis_rule();
    [[nodiscard]] bool parse_deletion_rule(element&& input);
    [[nodiscard]] bool parse_metathesis_rule(element&& input);
    [[nodiscard]] bool parse_reduplication_rule(element&& input);
    [[nodiscard]] bool parse_substitution_rule(element&& input);

    [[nodiscard]] auto parse_input() -> parse_result<element>;
    [[nodiscard]] auto parse_output() -> parse_result<element>;
    [[nodiscard]] auto parse_context() -> parse_result<rule::context>;

    [[nodiscard]] auto parse_input_element() -> parse_result<element>;
    [[nodiscard]] auto parse_input_elements() -> parse_result<element>;
    [[nodiscard]] auto parse_output_element() -> parse_result<element>;
    [[nodiscard]] auto parse_output_elements() -> parse_result<element>;

    [[nodiscard]] auto parse_boundaries() -> element;
    [[nodiscard]] auto parse_context_elements() -> parse_result<element>;

    [[nodiscard]] auto parse_decorated_element() -> parse_result<element>;
    [[nodiscard]] auto parse_decorated_elements() -> parse_result<element>;

    [[nodiscard]] auto parse_percent_alternatives() -> parse_result<element>;
    [[nodiscard]] auto parse_percent_class() -> parse_result<element>;
    [[nodiscard]] auto parse_percent_list() -> parse_result<element>;
    [[nodiscard]] auto parse_percent_elements() -> parse_result<element>;
    [[nodiscard]] auto parse_percent_element() -> parse_result<element>;

    [[nodiscard]] auto parse_simple_element() -> parse_result<element>;
    [[nodiscard]] auto parse_simple_element_class() -> parse_result<element>;
    [[nodiscard]] auto parse_simple_element_rhs() -> parse_result<element>;
    [[nodiscard]] auto parse_simple_element_class_literal() -> parse_result<element>;
    [[nodiscard]] auto parse_simple_element_list() -> parse_result<element>;
    [[nodiscard]] auto parse_simple_elements() -> parse_result<element>;

    ///
    /// SCA2 Grammar Parser
    ///

    /// Driver
    [[nodiscard]] bool zompist_parse(const std::u32string& z_text);

    /// SCA2 Preprocessor
    [[nodiscard]] bool zompist_process_rewrite_rules();

    void zompist_rewrite(std::u32string& z_text);
    void zompist_unrewrite(std::u32string& z_text);

    /// These all correspond to a rule in the grammar
    [[nodiscard]] bool zompist_top_level_statement();

    [[nodiscard]] bool zompist_rule(token&& first_token);
    [[nodiscard]] bool zompist_class_def(token&& class_name);

    [[nodiscard]] bool zompist_epenthesis_rule();
    [[nodiscard]] bool zompist_deletion_rule(element&& input);
    [[nodiscard]] bool zompist_metathesis_rule(element&& input);
    [[nodiscard]] bool zompist_substitution_or_reduplication_rule(element&& input);

    [[nodiscard]] auto zompist_input(token&& input) -> parse_result<element>;
    [[nodiscard]] auto zompist_output() -> parse_result<element>;
    [[nodiscard]] bool zompist_substitution_or_reduplication_output(rule& r);

    [[nodiscard]] auto zompist_char_or_category_list() -> parse_result<element>;
    [[nodiscard]] auto zompist_char_or_category() -> parse_result<element>;
    [[nodiscard]] auto zompist_category() -> parse_result<element>;
    [[nodiscard]] auto zompist_category_literal(bool needs_lbrack = true, location lbrack_pos = {}) -> parse_result<element>;

    [[nodiscard]] bool zompist_context(rule& r);
    [[nodiscard]] bool zompist_context_exception(rule& r);

    [[nodiscard]] auto zompist_decorated_context_elements() -> parse_result<element>;
    [[nodiscard]] auto zompist_decorated_context_element() -> parse_result<element>;
    [[nodiscard]] auto zompist_context_elements() -> parse_result<element>;
    [[nodiscard]] auto zompist_context_element() -> parse_result<element>;
    [[nodiscard]] auto zompist_char_or_context_category() -> parse_result<element>;
    [[nodiscard]] auto zompist_context_category_literal() -> parse_result<element>;
    [[nodiscard]] auto zompist_context_category_literal_member() -> parse_result<element>;

    [[nodiscard]] auto zompist_chars_raw(const char32_t* start_pos, location char_loc) -> element;
};
#undef DECLARE_TOKEN_TYPE
#undef CONSTEXPR_ASSERT

std::string enum_name(parser::rule_type t);
std::string enum_name(parser::element_type t);

} // namespace sca

template <>
struct fmt::formatter<sca::parser::location> {
    template <typename ctx>
    constexpr auto parse(ctx& c) { return c.begin(); }

    template <typename ctx>
    auto format(const sca::parser::location& l, ctx& c) { return fmt::format_to(c.out(), "[Line: {}, Column: {}]", l.line, l.col); }
};

template <>
struct fmt::formatter<sca::parser::range> {
    template <typename ctx>
    constexpr auto parse(ctx& c) { return c.begin(); }

    template <typename ctx>
    auto format(const sca::parser::range& r, ctx& c) { return fmt::format_to(c.out(), "[{}, {} -- {}, {}]", r.start.line, r.start.col, r.end.line, r.end.col); }
};

template <>
struct fmt::formatter<sca::parser::token> {
    template <typename ctx>
    constexpr auto parse(ctx& c) { return c.begin(); }

    template <typename ctx>
    auto format(const sca::parser::token& tok, ctx& c) { return fmt::format_to(c.out(), "{}", tok.str()); }
};

template <>
struct fmt::formatter<sca::parser::token_type> {
    template <typename ctx>
    constexpr auto parse(ctx& c) { return c.begin(); }

    template <typename ctx>
    auto format(const sca::parser::token_type& type, ctx& c) { return fmt::format_to(c.out(), "{}", sca::parser::token::str(type)); }
};

inline std::ostream& operator<<(std::ostream& os, sca::parser::range r) {
    os << "[" << r.start.line << ", " << r.start.col << " -- " << r.end.line << ", " << r.end.col << "]";
    return os;
}

inline std::ostream& operator<<(std::ostream& os, sca::parser::location l) {
    os << "[Line: " << l.line << ", Column: "
       << "" << l.col << "]";
    return os;
}

#endif // SCA_PARSER_HH
