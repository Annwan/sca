#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';
use v5.34;

use File::Copy;
use File::Basename;
use File::Path qw(make_path remove_tree);
use Cwd qw(getcwd);

# Constants and other important globals.
my $Y = "\033[33m";
my $R = "\033[m";
my $PROJ_ROOT = `readlink -f ${\(dirname __FILE__)}`; ${^CHILD_ERROR_NATIVE} and die "Unreachable at line " . __LINE__;
chomp $PROJ_ROOT;

# ############################################################### #
# Subroutines                                                     #
# ############################################################### #

sub abort($) {
    print STDERR "Error: $_[0]\n";

    # Stack trace:
    my $i = 1;
    while (my ($p, $file, $line, $sub) = caller($i++)) { print STDERR "    at $sub called from $file:$line\n"; }
    exit 1
}

# Get the basename of a file. Only works if the path contains '/'.
sub base_name($) { return $_[0] =~ /.*\/(.*)$/ ? $1 : $_[0] }

# This does exactly what you think it does.
sub cd($) {
    my $cwd = getcwd or abort "Could not get working directory: $!";
    if ($_[0] ne $cwd) { chdir $_[0] or abort "Could not cd into '$_[0]': $!" }
}

# This does exactly what you think it does.
sub cp($$) {
    my ($from, $to) = @_;
    copy $from, $to or abort "Could not copy file $Y${\($to =~ /.*\/(.*)$/)}$R from $Y$from$R to $Y$to$R"
}

# Create a directory if it doesn't already exist.
sub mkdirp($) { mkdir $_[0] or abort "Could not create directory $_[0]" if not -e $_[0] }

# Reset a directory.
sub reset_dir($) {
    remove_tree $_[0], { safe => 1 } if -e $_[0];
    mkdir $_[0] or abort "Could not create directory $_[0]";
}

sub sh($) {
    open FH, "$_[0] |" or abort "Shell command failed: '$_[0]'";
    print while (<FH>);
    ${^CHILD_ERROR_NATIVE} and abort "Shell command failed: '$_[0]'";
    close FH
}

# Delete the build files.
sub clean {
    remove_tree "$PROJ_ROOT/out", "$PROJ_ROOT/out-wasm", "$PROJ_ROOT/out-windows", {
        safe      => 1,
        keep_root => 1,
    };

    exit 0 unless scalar @ARGV > 1
}

sub watch {
    cd 'frontend';

    exec '/bin/bash', '-c', qq {
    ls src/*.html js/*.js css/*.scss | entr -np bash -c 'echo "[\$(date +%T)] Recompiling..." \\
        && sass css/sca.scss css/sca.css \\
        && sass css/syntax.scss css/syntax.css \\
        && cp src/main.html package/sca++-linux-x64/resources/app/src/main.html \\
        && cp src/syntax.html package/sca++-linux-x64/resources/app/src/syntax.html \\
        && cp js/page.js package/sca++-linux-x64/resources/app/js/page.js \\
        && cp js/syntax.js package/sca++-linux-x64/resources/app/js/syntax.js \\
        && cp css/sca.css package/sca++-linux-x64/resources/app/css/sca.css \\
        && cp css/syntax.css package/sca++-linux-x64/resources/app/css/syntax.css'
    } or abort "exec failed: $1"
}

# ############################################################### #
# Main                                                            #
# ############################################################### #

# Make sure we're in the right directory.
cd $PROJ_ROOT;

# Sanity check.
abort '$HOME not set!' unless -e $ENV{'HOME'};
my $HOME = $ENV{'HOME'};

# Option values.
my $sca_platform = 'linux';
my $sca_build_type = 'Release';
my $sca_deploy = 0;
my $sca_lib_only = 0;

# Process command line args.
foreach (@ARGV) {
    if ($_ eq 'linux') { $sca_platform = 'linux' }
    elsif ($_ eq 'wasm') { $sca_platform = 'wasm' }
    elsif ($_ eq 'windows') { $sca_platform = 'windows' }
    elsif ($_ eq 'Release' or $_ eq 'release') { $sca_build_type = 'Release' }
    elsif ($_ eq 'Debug' or $_ eq 'debug') { $sca_build_type = 'Debug' }
    elsif ($_ eq 'deploy') { $sca_deploy = 1 }
    elsif ($_ eq 'lib') { $sca_lib_only = 1 }
    elsif ($_ eq 'clean') { clean }
    elsif ($_ eq 'watch') { watch }
    else { abort "Unknown argument '${\($_)}'" }
}

# ############################################################### #
# Build libsca++                                                  #
# ############################################################### #

if ($sca_platform eq 'linux') {
    mkdirp 'out'; cd 'out';
    sh "cmake -DCMAKE_BUILD_TYPE='$sca_build_type' .. -GNinja";
    sh 'ninja';
}

elsif ($sca_platform eq 'windows') {
    # Set up the toolchain.
    $ENV{CC} = 'x86_64-w64-mingw32-gcc';
    $ENV{CXX} = 'x86_64-w64-mingw32-g++';
    $ENV{LD} = 'x86_64-w64-mingw32-ld';
    $ENV{CMAKE_TOOLCHAIN_FILE} = "$PROJ_ROOT/Windows.cmake";

    mkdirp 'out-windows'; cd 'out-windows';
    sh "cmake -DCMAKE_BUILD_TYPE='$sca_build_type' .. -GNinja";
    sh 'ninja'
}

elsif ($sca_platform eq 'wasm') {
    # Set up the toolchain.
    $ENV{CC} = '/usr/lib/emscripten/emcc';
    $ENV{CXX} = '/usr/lib/emscripten/em++';
    $ENV{LD} = '/usr/bin/wasm-ld';
    $ENV{CMAKE_TOOLCHAIN_FILE} = '/usr/lib/emscripten/cmake/Modules/Platform/Emscripten.cmake';

    mkdirp 'out-wasm'; cd 'out-wasm';
    sh " \\
    source '$HOME/libs/emsdk/emsdk_env.sh' && \\
    emcmake cmake -DCMAKE_FIND_ROOT_PATH=/ -DCMAKE_BUILD_TYPE='$sca_build_type' .. && \\
    emmake make -j \$(nproc)";
}


# ############################################################### #
# Build SCA++                                                     #
# ############################################################### #

exit 0 if $sca_lib_only == 1;
cd "$PROJ_ROOT/frontend";

# The directories and files to be excluded from the final package.
my $npx_ignores; $npx_ignores .= "--ignore='$_' "
    foreach qw(.idea build.sh src/electron/binding.cc src/web/binding.cc package/);

if ($sca_platform eq 'linux') {
    # Copy the dependencies.
    reset_dir 'deps/linux';
    cp '/usr/lib/libicuuc.so', 'deps/linux/';

    # Build the node module.
    sh 'node-gyp configure';
    sh 'node-gyp build';

    # Copy it into the 'native/' directory.
    reset_dir 'native';
    cp 'build/Release/sca.node', 'native/sca.node';

    # Package the app.
    sh " \\
    npx electron-packager . sca++ --platform=linux --arch=x64 \\
        --out=package --overwrite \\
        --icon ./assets/icon.svg \\
        $npx_ignores";
}

elsif ($sca_platform eq 'windows') {
    # Copy the build dependencies and prepare the output directories.
    reset_dir 'deps/windows';
    mkdirp './build-windows';
    cp "$HOME/libs/mingw/icu/lib64/icuuc.lib", 'deps/windows/';

    # Build the node module.
    cd 'build-windows';
    sh "cmake -DCMAKE_TOOLCHAIN_FILE='$PROJ_ROOT/Windows.cmake' -DCMAKE_BUILD_TYPE='$sca_build_type' -GNinja ..";
    sh 'ninja';

    # Copy the runtime dependencies to the right location.
    cd '..';
    reset_dir 'native';
    cp './build-windows/sca.node', 'native/sca.node';
    cp $_, "native/$_" foreach glob "$HOME/libs/mingw/icu/bin64/*.dll";

    # Package it.
    sh " \\
    npx electron-packager . sca++ --platform=win32 --arch=x64 \\
        --out=package --overwrite \\
        --icon ./assets/icon.svg \\
        $npx_ignores";
}

elsif ($sca_platform eq'wasm') {
    # Prepare the output directories
    mkdirp 'package/wasm'; cd 'package/wasm';

    # Determine the optimisation level and debug flags.
    my $cflags;
    if ($sca_build_type eq 'Release') { $cflags = '-O3' }
    elsif ($sca_build_type eq 'Debug') { $cflags = '-O0 -g -s WARN_UNALIGNED=1 -fsanitize=leak,undefined,address' }
    else { abort 'Unreachable' }

    # Compile the library.
    sh " \\
    source '$HOME/libs/emsdk/emsdk_env.sh' && \\
    emcc -Wall -Wextra -Wundef -Werror=return-type \\
        -Wno-unused-function -Wno-unused-parameter -Wno-unused-variable \\
        -Wno-empty-body -Wno-nonnull -fexceptions \\
        $cflags \\
        -std=c++2b \\
        '$PROJ_ROOT/frontend/src/web/'*.cc \\
        -L'$PROJ_ROOT/out-wasm/' -L'$PROJ_ROOT/out-wasm/3rdparty/fmt/' \\
        -lsca++ -lfmt \\
        -sUSE_ICU=1 -sNO_DISABLE_EXCEPTION_CATCHING -sNO_DISABLE_EXCEPTION_THROWING -sALLOW_MEMORY_GROWTH=1 --bind \\
        -o libsca++.html";

    # There's no packaging this.
    exit 0
}

# ############################################################### #
# Package SCA++                                                   #
# ############################################################### #

exit 0 if $sca_deploy == 0;

if ($sca_platform eq 'linux') {
    cd 'package';
    sh 'tar czvf sca++-linux-x64.tar.gz sca++-linux-x64';
}

elsif ($sca_platform eq 'windows') {
    cd 'package';
    unlink 'sca++-win64.zip' if -e 'sca++-win64.zip';
    sh 'zip -r sca++-win64.zip sca++-win32-x64'
}
