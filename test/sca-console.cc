#include "../lib/applier.hh"
#include "../lib/clopts/include/clopts.hh"

#include <cstdarg>
#include <iostream>

#ifndef WIN32
#    include <unistd.h>
#endif

using namespace command_line_options;

using options = clopts< // clang-format off
	option<"--rule", "Which rule to test", i64>,
	flag<"--bc", "Print the compiled rules and exit">,
	flag<"--classes", "Print the classes and exit">,
	flag<"--rules", "Print the rules and exit">,
	flag<"--stress-test", "Do a stress test w/ 1000 classes, 1000 rules, 6000 words">,
	flag<"--test-percentages", "Test if percentages are still working correctly">,
	flag<"--tokens", "Test if percentages are still working correctly">,
	flag<"--zompist", "Test SCA2 syntax">,
	help
>; // clang-format on
options::parsed_options opts;

__attribute__((format(printf, 1, 2)))
__attribute__((noreturn)) //
void die(const char* format, ...) {
    std::va_list args;
    va_start(args, format);
    std::vfprintf(stderr, format, args);
    std::fprintf(stderr, "\n");
    va_end(args);
    std::exit(1);
}

__attribute__((noreturn)) //
void handle_error(const std::string& err) {
    die("%s", err.c_str());
}

#ifndef WIN32
#    include <fcntl.h>
#    include <omp.h>
#    include <sys/mman.h>
#    include <sys/stat.h>

std::u32string map_file(const std::string& filename) {
    int fd = ::open(filename.c_str(), O_RDONLY);
    if (fd < 0) [[unlikely]]
        throw std::runtime_error("open(\"" + filename + "\") failed: " + ::strerror(errno));

    struct stat s {};
    if (::fstat(fd, &s)) [[unlikely]]
        throw std::runtime_error("fstat(\"" + filename + "\") failed: " + ::strerror(errno));
    auto sz = size_t(s.st_size);
    if (sz == 0) [[unlikely]]
        throw std::runtime_error("close(\"" + filename + "\") failed: " + ::strerror(errno));

    auto* mem = (char*) ::mmap(nullptr, sz, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
    if (mem == MAP_FAILED) [[unlikely]]
        throw std::runtime_error("mmap(\"" + filename + "\") failed: " + ::strerror(errno));

    if (::close(fd)) [[unlikely]]
        throw std::runtime_error("close(\"" + filename + "\") failed: " + ::strerror(errno));

    auto str = sca::to_utf32(std::string{mem, sz});
    if (::munmap(mem, sz)) [[unlikely]]
        throw std::runtime_error("munmap(\"" + filename + "\") failed: " + ::strerror(errno));
    return str;
}

[[noreturn]] void stress_test() {
    sca::parser p;
    auto        class_input = map_file("./test/stress_test_class_input.txt");
    auto        rules_input = map_file("./test/stress_test_rules_input.txt");
    auto [words, success]   = p.parse_words(map_file("./test/stress_test_words_input.txt"));
    if (!success) exit(1);
    if (!p.parse_classes(class_input)) exit(1);
    if (opts.has<"--rule">()) {
        static const std::vector<std::u32string> rules = {
            U"{s,m}//_#\n",
            U"i/j/_V\n",
            U"L/V/_\n",
            U"e//{V}r_#\n",
            U"v//V_V\n",
            U"u/o/_#\n",
            U"gn/nh/_\n",
            U"S/Z/V_V\n",
            U"c/i/F_t\n",
            U"c/u/B_t\n",
            U"p//V_t\n",
            U"ii/i/_\n",
            U"e//C_r{V}\n",
            U"lj/lh/_\n",
        };
        i64 idx = opts.get<"--rule">();
        if (idx < 0 || u64(idx) >= rules.size()) die("Bad option. Rule must be between 0 and %zu", rules.size());
        for (int i = 0; i < 1000; i++)
            if (!p.parse_rules(rules[idx])) exit(1);
    } else {
        if (!p.parse_rules(rules_input)) exit(1);
    }
    sca::applier a{p};
    auto         words2 = a.apply(words);
    std::exit(0);
}
#else
[[noreturn]] void stress_test() {
    std::cerr << "Stress test is not implemented on Windows\n";
    std::exit(1);
}
#endif

void test_percentages() {
    /// w contains 100 a's.
    static std::u32string         rules_input = U"a/80%{s, b, 20%c, 40%{q, 30%r, 10%{u, v, w}}, h}/_\n";
    static std::vector<sca::word> words       = {std::u32string{U"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"}};
    static constexpr u64          test_runs   = 1'000'000;

    u64 s{}, b{}, h{}, c{}, q{}, r{}, u{}, v{}, w{};
    u64 total_chars{};

    sca::parser p;
    if (!p.parse_rules(rules_input)) std::exit(1);

    if (opts.has<"--rules">()) {
        p.note({p.rules[0].input.pos.start, p.rules[0].ctx.pos.end}, "Rule");
        std::cout << sca::to_utf8(p.rules[0].str());
        std::exit(0);
    }

    for (u64 i = 1; i <= test_runs; i++) {
        if (!(i % 1000)) std::cout << "\033[G\033[K" << i << " / " << test_runs;
        auto words2 = sca::applier{p}.apply(words);
        for (auto ch : words2[0].data) {
            total_chars++;
            switch (ch) {
                case U's': s++; continue;
                case U'b': b++; continue;
                case U'h': h++; continue;
                case U'c': c++; continue;
                case U'q': q++; continue;
                case U'r': r++; continue;
                case U'u': u++; continue;
                case U'v': v++; continue;
                case U'w': w++; continue;
                default: ASSERT(false, "Applier produced incorrect character + '{}'", (char) ch);
            }
        }
    }

    std::cout << "\n";

    /// Expected result:
    std::printf("Letter   Got  \tExpected\n");
    std::printf("s        %2.2f\t%2.2f\n", (float(s) / float(total_chars)) * 100, 100 * .1333f);
    std::printf("b        %2.2f\t%2.2f\n", (float(b) / float(total_chars)) * 100, 100 * .1333f);
    std::printf("h        %2.2f\t%2.2f\n", (float(h) / float(total_chars)) * 100, 100 * .1333f);
    std::printf("c        %2.2f\t%2.2f\n", (float(c) / float(total_chars)) * 100, 100 * .2f);
    std::printf("q        %2.2f\t%2.2f\n", (float(q) / float(total_chars)) * 100, 100 * .4f * .6f);
    std::printf("r        %2.2f\t%2.2f\n", (float(r) / float(total_chars)) * 100, 100 * .4f * .3f);
    std::printf("u        %2.2f\t%2.2f\n", (float(u) / float(total_chars)) * 100, 100 * .4f * .1f * .3333f);
    std::printf("v        %2.2f\t%2.2f\n", (float(v) / float(total_chars)) * 100, 100 * .4f * .1f * .3333f);
    std::printf("w        %2.2f\t%2.2f\n", (float(w) / float(total_chars)) * 100, 100 * .4f * .1f * .3333f);
    std::fflush(stdout);
    std::exit(0);
}

[[noreturn]] void zompist() {
    sca::parser    p;
    std::u32string input = U"V=aeiou\n"
                           "L=āēīōū\n"
                           "C=ptcqbdgmnlrhs\n"
                           "F=ie\n"
                           "B=ou\n"
                           "S=ptc\n"
                           "Z=bdg\n"
                           "lh|lj\n"
                           "[sm]//_#\n"
                           "i/j/_V\n"
                           "L/V/_\n"
                           "e//Vr_#\n"
                           "v//V_V\n"
                           "u/o/_#\n"
                           "gn/nh/_\n"
                           "S/Z/V_V\n"
                           "c/i/F_t\n"
                           "c/u/B_t\n"
                           "p//V_t\n"
                           "ii/i/_\n"
                           "e//C_rV";
    if (!p.zompist_parse(input)) std::exit(1);

    if (opts.has<"--rules">()) {
        for (const auto& r : p.rules) {
            p.note(r.ctx.pos, "Defined here");
            std::cout << sca::to_utf8(r.str(0)) << "\n";
        }
        std::exit(0);
    }

    if (opts.has<"--bc">()) {
        std::vector<sca::parser::range> ranges;
        for (const auto& item : p.rules) ranges.push_back({item.input.pos.start, item.ctx.pos.end});
        auto fsms = sca::compiler::compile(p);
        for (u64 i = 0; i < fsms.size(); i++) {
            p.note(ranges[i], "Defined here");
            fsms[i].dump();
            if (i != fsms.size() - 1) std::cout << "\n";
        }
        std::exit(0);
    }

    auto [words, success] = p.parse_words(
        std::u32string(U"lector\n"
                       U"doctor\n"
                       U"focus\n"
                       U"jocus\n"
                       U"districtus\n"
                       U"cīvitatem\n"
                       U"adoptare\n"
                       U"opera\n"
                       U"secundus\n"
                       U"fīliam\n"
                       U"pōntem\n"));
    if (!success) exit(1);
    sca::applier a{p};
    auto         words2 = a.apply(words);
    for (auto& item : words2) p.zompist_unrewrite(item.data);
    for (u64 i = 0; i < words2.size(); i++)
        std::cout << sca::to_utf8(words[i].data) << " > " << sca::to_utf8(words2[i].data) << '\n';

    std::exit(0);
}

int main(int argc, char** argv) {
    std::u32string class_input =
        U"V={a,e,i,o,u}\n"
        U"L={ā,ē,ī,ō,ū}\n"
        U"C={p,t,c,q,b,d,g,m,n,l,r,h,s}\n"
        U"F={i,e}\n"
        U"B={o,u}\n"
        U"S={p,t,c}\n"
        U"Z={b,d,g}";
    std::u32string rules_input =
        U"{s,m}//_#\n"
        U"i/j/_V\n"
        U"L/V/_\n"
        U"e//{V}r_#\n"
        U"v//V_V\n"
        U"u/o/_#\n"
        U"gn/nh/_\n"
        U"S/Z/V_V\n"
        U"c/i/F_t\n"
        U"c/u/B_t\n"
        U"p//V_t\n"
        U"ii/i/_\n"
        U"e//C_r{V}\n"
        U"lj/lh/_\n"
        //U"o/u/~[g]_\n"
        ;

#ifndef WIN32
    assertion_error::use_colour = true;
    omp_set_num_threads(omp_get_max_threads() / 2 ?: 1);
#endif

    opts = options::parse(argc, argv);

    if (opts.has<"--zompist">()) zompist();
    if (opts.has<"--stress-test">()) stress_test();

    if (opts.has<"--tokens">()) {
        sca::parser p;
        p.parsing_rule = true;
        p.init(rules_input);
        //		for (const auto& tok : p.tokens()) p.note(tok.pos, tok.str());

        for (const auto& tok : p.tokens()) p.note(tok.pos, "{}", tok.str());
        return 0;
    }

    if (opts.has<"--classes">() || opts.has<"--rules">()) {
        sca::parser p;

        if (!p.parse_classes(class_input)) {
            std::cout << "Error parsing classes\n";
            exit(1);
        }

        if (opts.has<"--classes">()) {
            for (const auto& c : p.classes) {
                p.note(c.second.pos, "Defined here");
                std::cout << sca::to_utf8(sca::parser::class_to_str(c.first, c.second)) << "\n\n";
            }
            return 0;
        }

        if (!p.parse_rules(rules_input)) {
            std::cout << "Error parsing rules\n";
            exit(1);
        }

        for (const auto& r : p.rules) {
            p.note(r.ctx.pos, "Defined here");
            std::cout << sca::to_utf8(r.str(0)) << "\n";
        }

        return 0;
    }

    if (opts.has<"--bc">()) {
        sca::parser p;
        if (!p.parse_classes(class_input)) exit(1);
        if (!p.parse_rules(rules_input)) exit(1);

        std::vector<sca::parser::range> ranges;
        for (const auto& item : p.rules) ranges.push_back({item.input.pos.start, item.ctx.pos.end});
        auto fsms = sca::compiler::compile(p);
        for (u64 i = 0; i < fsms.size(); i++) {
            p.note(ranges[i], "Defined here");
            fsms[i].dump();
            if (i != fsms.size() - 1) std::cout << "\n";
        }
        return 0;
    }

    if (opts.has<"--test-percentages">()) test_percentages();

    sca::parser p;
    auto [words, success] = p.parse_words(
        std::u32string(
            U"lector\n"
                       U"cīvitatem\n"
                       U"doctor\n"
                       U"focus\n"
                       U"jocus\n"
                       U"districtus\n"
                       U"adoptare\n"
                       U"opera\n"
                       U"secundus\n"
                       U"fīliam\n"
                       U"pōntem\n"
            U"fygo"
            ));
    if (!success) exit(1);
    if (!p.parse_classes(class_input)) exit(1);
    if (!p.parse_rules(rules_input)) exit(1);

    sca::applier a{p, opts.has<"--bc">()};
    auto         words2 = a.apply(words);
    for (u64 i = 0; i < words2.size(); i++)
        std::cout << sca::to_utf8(words[i].data) << " > " << sca::to_utf8(words2[i].data) << '\n';
}
